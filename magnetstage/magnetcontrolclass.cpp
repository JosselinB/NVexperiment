#include "magnetcontrolclass.h"
#include "ximc.h"

#include <iostream>
#include <QTimer>

#include "magnetstage/magnetstagesettings.h"
#include <QMediaPlayer>

magnetcontrolclass::magnetcontrolclass(QObject *parent) :  QObject(parent)
{    
    //setting window
    settings = new magnetstagesettings();
    settingWindowTimer = new QTimer();

    connect(settingWindowTimer, SIGNAL(timeout()), SLOT(updateSettingsWindow()));

    //2d visualization window
    visualization2dim = new visualization2dplot();

    //borders window
    limitsStage = new magnetstagelimits();

    //music player
    ElevatorMusic = new QMediaPlayer();
    ElevatorMusic->setMedia(QUrl::fromLocalFile("C:/Users/peerscope/Documents/QtProjects/NVexp_from_scratch_2/magnetstage/media/Elevator_Music3min.mp3"));
    ElevatorMusic->setVolume(100);

}

magnetcontrolclass::~magnetcontrolclass()
{

}

void magnetcontrolclass::connect2motors()
{
    const int probe_flags = ENUMERATE_PROBE | ENUMERATE_NETWORK;
    //const char* enumerate_hints = "addr=192.168.1.1,172.16.2.3";
    const char* enumerate_hints = "addr=127.0.0.1";

    //const char* enumerate_hints = "addr="; // this hint will use broadcast enumeration, if ENUMERATE_NETWORK flag is enabled

    //set bindy (network) keyfile. Must be called before any call to "enumerate_devices" or "open_device" if you
    //wish to use network-attached controllers. Accepts both absolute and relative paths, relative paths are resolved
    //relative to the process working directory. If you do not need network devices then "set_bindy_key" is optional.

    set_bindy_key("keyfile.sqlite");

    //  iwas stimmt hier so gar nicht (Socket: connect operation failed with error 10061) adresse enumarate_hints
    //device enumeration function. Returns an opaque pointer to device enumeration data.
    std::cout << "searching for devices..." << std::endl;
    devenum = enumerate_devices(probe_flags, enumerate_hints);

    //gets device count from device enumeration data
    int names_count = get_device_count(devenum);
    std::cout << "done searching: number of found devices: " << names_count << std::endl;

    //terminate if there are no connected devices
    if (names_count <= 0)
    {
        std::cout << "no devices found" << std::endl;
        openMotors = false;
        return;
    }

    //the motor get named (this names are used later to call the diffrents motors)
    strcpy_s(name_rotStageBig, get_device_name(devenum, 2));
    strcpy_s(name_rotStageSmall, get_device_name(devenum, 1));
    strcpy_s(name_transStage, get_device_name(devenum, 0));

    //Free memory used by device enumeration data
    free_enumerate_devices(devenum);

    //open motors
    std::cout << "open devices..." << std::endl;

    rotStageBig = open_device(name_rotStageBig);
    if (rotStageBig == device_undefined) {
        std::cout << "error: big motor doesn't open..." << std::endl;
        return;
    }
    else {
        //std::cout << "big motor is open." << std::endl;
    }

    rotStageSmall = open_device(name_rotStageSmall);
    if (rotStageSmall == device_undefined) {
        std::cout << "error: small motor doesn't open..." << std::endl;
        return;
    }
    else {
        //std::cout << "small motor is open" << std::endl;
    }

    transStage = open_device(name_transStage);
    if (transStage == device_undefined) {
        std::cout << "error: distance motor doesn't open..." << std::endl;
        return;
    }
    else {
        //std::cout << "distance motor is open" << std::endl;
    }
    std::cout << "success, all motors are open." << std::endl;
    openMotors = true;
}

void magnetcontrolclass::disconnect2motors()
{
    //close motors
    close_device(&rotStageBig);
    //std::cout << "big motor shut down." << std::endl;
    close_device(&rotStageSmall);
    //std::cout << "small motor shut down. " << std::endl;
    close_device(&transStage);
    //std::cout << "distance motor shut down. " << std::endl;
    openMotors = false;
    std::cout << "Magnet Stage closed!" << std::endl;
    if(settings->isVisible()){
        settings->close();
    }
    if(visualization2dim->isVisible()){
        visualization2dim->close();
    }
}

bool magnetcontrolclass::getStateMotors()
{
    return openMotors;
}


//get the positions of the motors and make them accessible in the UI
//reminde: the get_status function returns the position in steps not degree or millimeter etc.!!!
//360deg == 24000steps
double magnetcontrolclass::getPositionRotStageBig()
{
    get_status(rotStageBig, &status_rotStageBig);
    double position_big = status_rotStageBig.CurPosition;
    return (3.*position_big/2.)/100;
}
//360deg == 36000steps
double magnetcontrolclass::getPositionRotStageSmall()
{
    get_status(rotStageSmall, &status_rotStageSmall);
    double position_small = status_rotStageSmall.CurPosition;
    return -position_small/100;
}
//50mm == 40000step
double magnetcontrolclass::getPositionTransStage()
{
    get_status(transStage, &status_transStage);
    double position_distance = status_transStage.CurPosition;
    return -1.25*position_distance/1000;
}


//move functions
void magnetcontrolclass::setMoveValues(double move2posTransStage_mm, double move2posRotStageSmall_deg, double move2posRotStageBig_deg)
{
    if(move2posTransStage_mm < getPositionTransStage()){
        small2big = false;
    }else{
        small2big = true;
    }
    this->move2posTransStage_mm = move2posTransStage_mm;
    this->move2posRotStageSmall_deg = move2posRotStageSmall_deg;
    this->move2posRotStageBig_deg = move2posRotStageBig_deg;

    std::cout << "start" << std::endl;
    ElevatorMusic->play();
}

void magnetcontrolclass::move_transStage(double move2posTransStage_mm)
{
    command_move(transStage,-(move2posTransStage_mm*1000/1.25),0);
}

void magnetcontrolclass::move_rotStageSmall(double move2posRotStageSmall_deg)
{
    command_move(rotStageSmall,-move2posRotStageSmall_deg*100,0);
}

void magnetcontrolclass::move_rotStageBig(double move2posRotStageBig_deg)
{
    command_move(rotStageBig, 2.*move2posRotStageBig_deg*100/3.,0);
}

bool magnetcontrolclass::checkMagnetstageLimits(double move2posTransStage_mm, double move2posRotStageSmall_deg, double move2posRotStageBig_deg)
{
    std::cout << "distance: " << move2posTransStage_mm << ", small rotor: " << move2posRotStageSmall_deg << ", big rotor: " << move2posRotStageBig_deg << std::endl;
    // fitted function:
    // f(x,z) = a*(x-b)^2+c*(z-d)^2+e = y
    double a = 0.0160961;
    double b = -0.00063412;
    double c = 0.00887396;
    double d = -14.2215;
    double e = -17.0023;

    double border = a*pow((move2posRotStageSmall_deg-b),2)+c*pow((move2posRotStageBig_deg-d),2)+e;
    if(border<0) border=0;
    if(border>50) border=50;

    // +1 to minimize the risk of a collision (reason: the used function is fittet so values can be upper or lower to the beloning determind value)
    if((border+1) < move2posTransStage_mm) return true;
    return false;
}

//stop and move motors until they're are in the right position
void magnetcontrolclass::stopMotors()
{
    command_sstp(rotStageBig);
    command_sstp(rotStageSmall);
    command_sstp(transStage);
}

bool magnetcontrolclass::moveUntilPosition()
{
    //updates the 2D Plot if the belonging window is open
    if(visualization2dim->isVisible()){
        update2DVisualization();
    }

    //the motors are't moving until they're at the given position. They're moving as
    //long as the programm starts with the next command line.
    //To solve the problem this function is called (by using a QTimer) as long as the
    //positions for all three motors are reached.

    //case 1: the distance increase
    if(small2big){
        if (!simultanMovement){
            //it first starts to move the transStage motor, when this is done it starts to
            //move the next motor etc.
            move_transStage(move2posTransStage_mm);
            if((getPositionTransStage() > (move2posTransStage_mm - 0.02)) && (getPositionTransStage() < (move2posTransStage_mm + 0.02))){
                move_rotStageBig(move2posRotStageBig_deg);
                if((getPositionRotStageBig() > (move2posRotStageBig_deg - 0.02)) && (getPositionRotStageBig() < (move2posRotStageBig_deg + 0.02))){
                    move_rotStageSmall(move2posRotStageSmall_deg);
                }
            }
        }else{
            //note: the distance motor is in this case used first
            move_transStage(move2posTransStage_mm);
            if((getPositionTransStage() > (move2posTransStage_mm - 0.02)) && (getPositionTransStage() < (move2posTransStage_mm + 0.02))){
                //std::cout << "Polar: " << posPolar_cdegree << ", Azimutal: " << posAzimutal_cdegree << std::endl;
                move_rotStageBig(move2posRotStageBig_deg);
                move_rotStageSmall(move2posRotStageSmall_deg);
            }
        }

    //case 2: the distance decrease
    }else{
        if (!simultanMovement){
            //it first starts to move the rotation motors, when this is done it starts to
            //move the next motor etc.
            move_rotStageBig(move2posRotStageBig_deg);
            if((getPositionRotStageBig() > (move2posRotStageBig_deg - 0.02)) && (getPositionRotStageBig() < (move2posRotStageBig_deg + 0.02))){
                move_rotStageSmall(move2posRotStageSmall_deg);
                if((getPositionRotStageSmall() > (move2posRotStageSmall_deg - 2.0)) && (getPositionRotStageSmall() < (move2posRotStageSmall_deg + 2.0))){
                        move_transStage(move2posTransStage_mm);
                }
            }
        }else{
            //note: the distance motor is in this case used first
            move_rotStageBig(move2posRotStageBig_deg);
            move_rotStageSmall(move2posRotStageSmall_deg);
            if((getPositionRotStageBig() > (move2posRotStageBig_deg - 0.02)) && (getPositionRotStageBig() < (move2posRotStageBig_deg + 0.02)) &&
                    (getPositionRotStageSmall() > (move2posRotStageSmall_deg - 2.0)) && (getPositionRotStageSmall() < (move2posRotStageSmall_deg + 2.0))){
                move_transStage(move2posTransStage_mm);
            }
        }
    }

    if((getPositionTransStage() > (move2posTransStage_mm - 0.02)) && (getPositionTransStage()
             < (move2posTransStage_mm + 0.02)) && (getPositionRotStageBig() >
            (move2posRotStageBig_deg - 0.02)) && (getPositionRotStageBig() <
            (move2posRotStageBig_deg + 0.02)) &&(getPositionRotStageSmall() > (move2posRotStageSmall_deg - 0.02))
            && (getPositionRotStageSmall() < (move2posRotStageSmall_deg + 0.02))){
            ElevatorMusic->stop();
            std::cout << "stop" << std::endl;
            return true;
    }
    return false;
}

void magnetcontrolclass::setSimultanMovement(bool simultanMovement)
{
    this->simultanMovement = simultanMovement;
}


// more information windows
// - settings
void magnetcontrolclass::openSettings()
{
    settings->show();
    settingWindowTimer->start(1000);
}

// - 2d visualization
void magnetcontrolclass::open2DVisualization()
{
    visualization2dim->show();
    visualization2dim->smallRotStagePlot(getPositionRotStageSmall(),getPositionTransStage());
    visualization2dim->bigRotStagePlot(getPositionRotStageBig(),getPositionTransStage());
}

void magnetcontrolclass::update2DVisualization()
{
    visualization2dim->smallRotStagePlot(getPositionRotStageSmall(),getPositionTransStage());
    visualization2dim->bigRotStagePlot(getPositionRotStageBig(),getPositionTransStage());
}

// - borders visualization
void magnetcontrolclass::openBorders()
{
    limitsStage->show();
}

void magnetcontrolclass::updateSettingsWindow()
{
    //set the current position
    settings->updateUiStageInformationPosition(getPositionRotStageSmall(),getPositionRotStageBig(),getPositionTransStage());

    //set current temperature
    get_status(rotStageSmall, &status_rotStageSmall);
    get_status(rotStageBig, &status_rotStageBig);
    get_status(transStage, &status_transStage);
    settings->updateUiStageInformationTemperature(status_rotStageSmall.CurT, status_rotStageBig.CurT,status_transStage.CurT);

    //set current voltage and current
    get_engine_settings(rotStageSmall, &engine_settings_rotStageSmall);
    get_engine_settings(rotStageBig, &engine_settings_rotStageBig);
    get_engine_settings(transStage, &engine_settings_transStage);
    settings->updateUiStageInformationVoltage(engine_settings_rotStageSmall.NomVoltage, engine_settings_rotStageBig.NomVoltage, engine_settings_transStage.NomVoltage);
    settings->updateUiStageInformationCurrent(engine_settings_rotStageSmall.NomCurrent, engine_settings_rotStageBig.NomCurrent, engine_settings_transStage.NomCurrent);

    int changeZeroPoint = settings->getZeroPointState();
    switch(changeZeroPoint){
    case 0: break;
    case 1: command_zero(rotStageBig);
            settings->setZeroPointStateBack();
            std::cout << "New zero Point setted!" << std::endl;
            break;
    case 2: command_zero(rotStageSmall);
            settings->setZeroPointStateBack();
            std::cout << "New zero Point setted!" << std::endl;
            break;
    case 3: command_zero(rotStageBig);
            command_zero(rotStageSmall);
            settings->setZeroPointStateBack();
            break;
    default: break;
    }

    if(!settings->isVisible()){
        settingWindowTimer->stop();
    }
}

