#include "magnetstagelimits.h"
#include "ui_magnetstagelimits.h"

#include <QtDataVisualization/Q3DSurface>
#include <iostream>

int sampleCountX = 120;
int sampleCountY = 50;
int sampleCountZ = 70;

magnetstagelimits::magnetstagelimits(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::magnetstagelimits)
{
    ui->setupUi(this);

    QWidget::setWindowTitle("limits");

    magnetstageLimitsPlot = new QtDataVisualization::Q3DSurface();
    QWidget *limitsPlot = QWidget::createWindowContainer(magnetstageLimitsPlot);
    ui->limits3dVisualization->addWidget(limitsPlot);
    magnetstageLimitsPlot->setFlags(magnetstageLimitsPlot->flags()^Qt::FramelessWindowHint);

    magnetstageLimitsPlot->setAxisX(new QtDataVisualization::QValue3DAxis);
    magnetstageLimitsPlot->setAxisY(new QtDataVisualization::QValue3DAxis);
    magnetstageLimitsPlot->setAxisZ(new QtDataVisualization::QValue3DAxis);

    magnetstageLimitsPlot->axisX()->setRange(-65,65);
    magnetstageLimitsPlot->axisY()->setRange(-5,60);
    magnetstageLimitsPlot->axisZ()->setRange(-5,75);

    magnetstageLimitsPlot->axisX()->setTitle("angle small motor in [deg]");
    magnetstageLimitsPlot->axisX()->setTitleVisible(true);
    magnetstageLimitsPlot->axisY()->setTitle("Distance(mm)");
    magnetstageLimitsPlot->axisY()->setTitleVisible(true);
    magnetstageLimitsPlot->axisZ()->setTitle("angel big motor in [deg]");
    magnetstageLimitsPlot->axisZ()->setTitleVisible(true);

    //plot the data in the coordinate system
    QtDataVisualization::QSurfaceDataProxy *borderProxy = new  QtDataVisualization::QSurfaceDataProxy();
    QtDataVisualization::QSurface3DSeries *borderSeries = new QtDataVisualization::QSurface3DSeries(borderProxy);

    borderSeries->setDrawMode(QtDataVisualization::QSurface3DSeries::DrawSurfaceAndWireframe);

    magnetstageLimitsPlot->addSeries(borderSeries);

    QtDataVisualization::QSurfaceDataArray *dataArray = new QtDataVisualization::QSurfaceDataArray;
    dataArray->reserve(sampleCountY);



    // fitted function:
    // f(x,z) = a*(x-b)^2+c*(z-d)^2+e = y
    // a = 0.0161 pm 0.0001
    // b = -0.0006 pm 0.0884
    // c = 0.0089 pm 0.0003
    // d = -14.2215 pm 1.6050
    // e = -17.0023 pm 0.6788


    for (int i=0; i<sampleCountZ; i++){
        QtDataVisualization::QSurfaceDataRow *newRow = new QtDataVisualization::QSurfaceDataRow(sampleCountX+1);
        double z = i;
        int index = 0;
        for(int j=0; j<sampleCountX+1; j++){
            double x = j-60;
            double y = a*pow((x-b),2)+c*pow((z-d),2)+e;
            if(y<0) y=0;
            if(y>50) y=50;
            (*newRow)[index++].setPosition(QVector3D(x,y,z));
        }
        *dataArray << newRow;
    }
    borderProxy->resetArray(dataArray);

}

magnetstagelimits::~magnetstagelimits()
{
    delete ui;
}

void magnetstagelimits::on_checkPossibleDistance_clicked()
{
    double limit = a*pow((ui->smallRotStagePos->value()-b),2)+c*pow((ui->bigRotStagePos->value()-d),2)+e;
    if(limit<0) limit=0;
    if(limit>50) limit=50;
    ui->transStagePos->display(limit);
}
