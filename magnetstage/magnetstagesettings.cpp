#include "magnetstagesettings.h"
#include "ui_magnetstagesettings.h"

#include <QMessageBox>
#include <iostream>

magnetstagesettings::magnetstagesettings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::magnetstagesettings)
{
    ui->setupUi(this);
    QWidget::setWindowTitle("settings");
}

magnetstagesettings::~magnetstagesettings()
{
    delete ui;
}

//update position, temperature, current and voltage secondly
void magnetstagesettings::updateUiStageInformationPosition(double posSmallRotStage, double posBigRotStage, double posTransStage)
{
    ui->positionTransStage->setNum(posTransStage);
    ui->positionSmallRotStage->setNum(posSmallRotStage);
    ui->positionBigRotStage->setNum(posBigRotStage);
}

void magnetstagesettings::updateUiStageInformationTemperature(double tempSmallRotStage, double tempBigRotStage, double tempTransStage)
{
    ui->temperatureTransStage->setNum(tempTransStage/10);
    ui->temperatureSmallRotStage->setNum(tempSmallRotStage/10);
    ui->temperatureBigRotStage->setNum(tempBigRotStage/10);
}

void magnetstagesettings::updateUiStageInformationVoltage(double voltSmallRotStage, double voltBigRotStage, double voltTransStage)
{
    ui->voltageTransStage->setNum(voltTransStage/10);
    ui->voltageSmallRotStage->setNum(voltSmallRotStage/10);
    ui->voltageBigRotStage->setNum(voltBigRotStage/10);
}

void magnetstagesettings::updateUiStageInformationCurrent(double curSmallRotStage, double curBigRotStage, double curTransStage)
{
    ui->currentTransStage->setNum(curTransStage/10);
    ui->currentSmallRotStage->setNum(curSmallRotStage/10);
    ui->currentBigRotStage->setNum(curBigRotStage/10);
}

bool magnetstagesettings::getMusicState()
{
    return musicOnOff;
}

int magnetstagesettings::getZeroPointState()
{
    //Note 0: nothing changed, 1: big motor, 2: small motor, 3: both motors
    return zeroPoint;
}

void magnetstagesettings::setZeroPointStateBack()
{
    zeroPoint = 0;
}

//set the current position as new zero points. not recommended!!!
void magnetstagesettings::on_changeZeroPoints_clicked()
{
    //multiple warnings because if the zero points are set new the borders has the be determind again!!
    QMessageBox reset_zero_points;
    reset_zero_points.setText("WARNING!");
    reset_zero_points.setInformativeText("Do you really want to change the zero points (not recommended!)? Saving them will delete the previous zero point. The boardes have to determind afterwards!");
    reset_zero_points.addButton(tr("Cancel"), QMessageBox::NoRole);
    reset_zero_points.addButton(tr("Big Motor"), QMessageBox::NoRole);
    reset_zero_points.addButton(tr("Small Motor"), QMessageBox::NoRole);
    reset_zero_points.addButton(tr("Both Motors"), QMessageBox::NoRole);

    int answer = reset_zero_points.exec();
    if (answer == 1){
        QMessageBox::StandardButton reply = QMessageBox::question(this,"WARNING!","Are you sure?",QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::Yes){
            //Note 0: nothing changed, 1: big motor, 2: small motor, 3: both motors
            zeroPoint = 1;
        }else {
            QMessageBox::about(this,"WARNING!","No changes made.");
        }
    }
    if (answer == 2){
        QMessageBox::StandardButton reply2 = QMessageBox::question(this,"WARNING!","Are you sure?",QMessageBox::Yes|QMessageBox::No);
        if(reply2 == QMessageBox::Yes){
            //Note 0: nothing changed, 1: big motor, 2: small motor, 3: both motors
            zeroPoint = 2;
        }else {
            QMessageBox::about(this,"WARNING!","No changes made.");
        }
    }
    if(answer == 3){
        QMessageBox::StandardButton reply3 = QMessageBox::question(this,"WARNING!","Are you sure?",QMessageBox::Yes|QMessageBox::No);
        if(reply3 == QMessageBox::Yes){
            //Note 0: nothing changed, 1: big motor, 2: small motor, 3: both motors
            zeroPoint = 3;
        }else {
            QMessageBox::about(this,"WARNING!","No changes made.");
        }
    }
}

void magnetstagesettings::on_musicCheckBox_clicked(bool musicState)
{
    std::cout << "work in progress..." << std::endl;
}
