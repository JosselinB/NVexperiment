#ifndef QCPSEQUENCEDATA_H
#define QCPSEQUENCEDATA_H
#include "qcustomplot.h"
#include <unordered_map>
#include <iostream>


enum class Edge
{
    noEdge,
    rising,
    falling
};

struct edgeProp
{
    double timestamp;
    QCPItemLine* arrow;
};
typedef edgeProp edgeProp;

struct varEdgeProp
{
    edgeProp maxProps;
    int stepsNb;
    double minTimestamp;
    double maxTimestamp;
};

class QSeqEditor;

class QCPSequenceData : public QCPGraph
{
public:
    QCPSequenceData(QCPAxis* keyAxis, QCPAxis* valueAxis, QSeqEditor* parentEditor);

    std::vector<edgeProp> risingEdges;
    std::vector<edgeProp> fallingEdges;

    std::list<std::string> variableT_names;
    void parseAndPlot();
    void mousePressEvent(QMouseEvent* event, const QVariant& arg_details);
    bool checkArrowsRemaining();
    //void generateTimestampsFromVariableT(int length, int nb_of_var_T, std::unordered_map<std::string, varEdgeProp> map_nameToProps);


    QSeqEditor* parentEditor;


};

#endif // QCPSEQUENCEDATA_H
