
#include "m2icard.h"

static const uint32 M2iMemorySizeTotal = 1<<29; //512MiB of total onboard RAM memory
static const uint32 M2iMaxDivider = 256;

typedef std::vector< std::vector<int> > multichanEdgeData;

int next_power_of_2(int A)
{
    if (A == 0)
      return 0;

    int integLog2 = (int)log2(A);
    if (1 << integLog2 == A)
        return A;
    else
        return 1 << (integLog2 +1);
}



template <typename T>
std::string integ_to_binStr(T a, bool comma = false)
{
    if(sizeof(a)>8)
    {
        std::cout << "Error: type size larger than 8 bytes!";
        return "?";
    }
    uint64_t M;
    uint64_t aL = (uint64_t) a;
    std::string st;

    for(int i = sizeof(T)*8-1; i>=0; --i)
    {
        M=1ULL<<i;
        aL&M? st.append("1"):st.append("0");
        if((i>0)&&(i%4==0)&&comma){ st.append(",");}
    }
    return st;
}

template<typename T>
T min(T x1, T x2)
{
    return x1<x2? x1: x2;
}

template<typename T>
T max(T x1, T x2)
{
    return x1>x2? x1: x2;
}


std::vector<int> timestampsVec_doubleToInt(const std::vector<edgeProp>& seqData, double step)
{
    std::vector<int> result;
    result.resize(seqData.size());

    unsigned int u;
    for( int k = 0; k<seqData.size(); ++k)
    {
        u = lround(seqData[k].timestamp/step);
        result[k] = u;
    }

    return result;
}


// examines what edges there are in successive 64bit chunks, makes a corresponding
// 64bit word per channel and parallelize them into the output buffer
// vector timestamps_rise and timestamps_fall are just indices of edges as INTEGERS,
// i.e. the timestamps in second will be this integer multiplying by time step
void translateTimestampsToParallelAndPutInBuffer(int seqLength,//length in samples/time points
                                                 int chanNb,
                                                 multichanEdgeData& timestamps_rise,
                                                 multichanEdgeData& timestamps_fall,
                                                 int16_t* pBuffer)
{
    if (seqLength%64 != 0)
    {
        std::cout << "Error: Length of sequence should be a multiple of 64 !!" << std::endl;
        return;
    }

    //help-vectors to store transitions found within one 64 bit chunk, one vector of ints for each channel
    //multichanEdgeData risingEdgesInChunk (chanNb);
    std::vector<int> risingEdgesInChunk;
    //multichanEdgeData fallingEdgesInChunk (chanNb);
    std::vector<int> fallingEdgesInChunk;

    //figure out how many channels to activate in total to get a valid number (1,2,4,8,16)
    int actChanNb = next_power_of_2(chanNb);

    //vector used to store temporary 64bit sequence chunk of
    //successive logic levels for each channel before parallelizing these chunks
    //size of the vector is the number activated channels
    std::vector<uint64_t> binChunkVec;
    binChunkVec.resize(actChanNb);
    //complete the unused channels with 0s
    for (int c = chanNb; c < actChanNb; ++c)
    {
        binChunkVec[c] = 0ULL;
    }

    int start_t, stop_t, eRise, eFall, rising_nb, falling_nb, timeInd;
    int index = 0;
    int found_val;
    std::vector<int>::iterator found_ind;
    uint64_t lvl, word;
    bool lvl_changed;

    std::vector<uint64_t> lvlAtEndOfLastChunk(chanNb, 0ULL);

    //predicate for finding edges; between start and stop and after the preceding found value
    auto test_timestampInChunck = [&start_t, &stop_t, &found_val](int T){return (T>=start_t)&&(T<=stop_t)&&(T>found_val);};

    for (int i=0; i<seqLength/64; ++i )//go through the entire length of the sequence
    {
        start_t = i*64;
        stop_t = start_t+63;

        // ############### for debugging ###############
        //std::cout << "Chunk " << i << ":" << std::endl;

        for (int ch=0; ch<chanNb; ++ch) //for each channel, with a max number of channel of 16
        {
            //reset the vectors of edges index for this chunk
            risingEdgesInChunk.clear();
            fallingEdgesInChunk.clear();

            //look what edges are in the 64 cycle long time interval
            //1. find rising edges
            found_val = -1;
            do
            {
                found_ind = std::find_if(timestamps_rise[ch].begin(), timestamps_rise[ch].end(), test_timestampInChunck);
                if (found_ind != timestamps_rise[ch].end())
                {
                    found_val = *found_ind;
                    risingEdgesInChunk.push_back(found_val);
                }
            } while (found_ind != timestamps_rise[ch].end());

            found_val = -1;
            //2. find falling edges
            do
            {
                found_ind = std::find_if(timestamps_fall[ch].begin(), timestamps_fall[ch].end(), test_timestampInChunck);
                if (found_ind != timestamps_fall[ch].end())
                {
                    found_val = *found_ind;
                    fallingEdgesInChunk.push_back(found_val);
                }

            } while (found_ind != timestamps_fall[ch].end());

            // ############### for debugging ###############
            /*
            //print all rising and falling edges found in the chunk
            auto printVec = [](int K){std::cout << K << "  ";};
            std::cout << "Rising edges found: ";
            for_each(risingEdgesInChunk.begin(), risingEdgesInChunk.end(), printVec);
            std::cout << std::endl;
            std::cout << "Falling edges found: ";
            for_each(fallingEdgesInChunk.begin(), fallingEdgesInChunk.end(), printVec);
            std::cout << std::endl;
            */

            //then build up a 64bit word corresponding to these transitions
            //Least Significant Bit corresponds to time closest to 0
            eRise = 0;
            eFall = 0;
            word = 0ULL;
            lvl = lvlAtEndOfLastChunk[ch];
            //first a sanity check: cannot have a difference of more than 1 rising or 1 falling edge between
            //the amount of edges found in the chunk

            if (std::abs((double)risingEdgesInChunk.size() - (double)fallingEdgesInChunk.size() ) > 1.1)
            {
                //std::cout << "Nb of rising edges: " << risingEdgesInChunk.size() << ", Nb of falling edges: " << fallingEdgesInChunk.size() << std::endl;
                std::cout << "Error: the difference in amount of rising and falling edges is " << std::abs((double)risingEdgesInChunk.size() - (double)fallingEdgesInChunk.size()) << ". This is too high; impossible sequence !!" << std::endl;
                return;
            }

            rising_nb = risingEdgesInChunk.size();
            falling_nb = fallingEdgesInChunk.size();
            if (rising_nb == 0 && falling_nb == 0) //no edges where found at all
            {
                if(lvl)
                    word = 0xFFFFFFFFFFFFFFFF; //64 1s
                    //else word stays equal to 64 0s
            }
            else if (rising_nb == 1 && falling_nb == 0)//only one rising edge found; it means the level is low and changes to high
            {
                index = risingEdgesInChunk[0]%64;
                for (uint8_t b = index; b<64 ; ++b)
                {
                    word += 1ULL<<b;
                }
                lvlAtEndOfLastChunk[ch] = 1ULL;//remember the level at the end of the chunk for this channel
            }
            else if (rising_nb == 0 && falling_nb == 1)//only one falling edge found; it means the level is high and changes to low
            {
                index = fallingEdgesInChunk[0]%64;
                for (uint8_t b = 0; b<index ; ++b)
                {
                    word += 1ULL<<b;
                }
                lvlAtEndOfLastChunk[ch] = 0ULL;//remember the level at the end of the chunk for this channel
            }
            else //at least 1 rising and 1 falling edge found
            {
                for(int b = 0; b<64; ++b)
                {
                    timeInd = 64*i + b;
                    lvl_changed = false;

                    if(eRise < rising_nb) //check if we haven't already seen all rising edges
                    {
                        if(timeInd == risingEdgesInChunk[eRise])//if there is a rising edge at this bit
                        {
                            lvl = 1ULL; //change level to 1
                            word += (lvl<<b);
                            ++eRise;
                            lvl_changed = true;
                        }
                    }

                    if(eFall < falling_nb) //check if we haven't already seen all falling edges
                    {
                        if (timeInd == fallingEdgesInChunk[eFall]) //if there is a falling edge at this bit
                        {
                            lvl = 0ULL; //changes level to 0
                            ++eFall;
                            lvl_changed = true;
                        }
                    }

                    if(!lvl_changed) //otherwise just keep the current level
                    {
                        word += (lvl<<b);
                    }
                }
                lvlAtEndOfLastChunk[ch] = lvl;//remember the level at the end of the chunk for this channel
            }
            //finally store the obtained 64bit words for this channel
            binChunkVec[ch] = word;
        }


        // ############### for debugging ###############
        //print content of vector with 64bit chunks
        /*
        std::cout << "data in binChunkVec\n";
        for (int j=0; j<binChunkVec.size(); ++j) { std::cout << "ch" << j << ": " << integ_to_binStr(binChunkVec[j], true) << std::endl;}
        */

        //now parallelize the chanNb 64bit chunk into (64bit*chanNb/16bit =) 4*chanNb 16bit words

        if(actChanNb<=16)//if the number of channel is valid
        {
            int m = 0;
            int16_t two_bytes = 0;
            for (int shift = 0; shift < 64; ++shift)
            {
                for(int aCh = 0; aCh < actChanNb; ++aCh)
                {
                    if((uint64_t)(1ULL<<shift) & binChunkVec[aCh])
                        two_bytes += (int16_t)(1<<(m%16));
                    if(m%16==15)
                    {
                        *pBuffer++ = two_bytes; // same as *(ptr++)
                        //std::cout << "new int16: " << integ_to_binStr(two_bytes, true) << std::endl;
                        two_bytes = 0;
                    }
                    ++m;
                }
            }
        }
        else
        {
            std::cout << "Number of channels not valid !" << std::endl;
        }
    }
}


//input: sorted vector of (increasing) run lengths
//1 tranfer means writing to a certain amount of segments in M2i memory
//because we need the END segment, chucks of 2 is not useful so chunks should be
std::vector<uint_triplet> chunks_from_length_set(std::vector<uint32> lengths)
{
    std::vector<uint_triplet> res;

    while(lengths.size()>0)
    {
        //get the divider that will accomodate the longest(last) run length into 1 segment
        uint32 maxDiv = min(M2iMemorySizeTotal/next_power_of_2(lengths[lengths.size()-1]), M2iMaxDivider);
        uint32 segSize = M2iMemorySizeTotal/maxDiv;
        uint32 nextSegSize = segSize/2;
        //search, starting from the largest length backwards, for the index k of the first length
        //that would fit into a smaller segment (smaller by a factor 2 or more)
        //i.e. would be compatible with a larger divider
        int k;
        for(k=lengths.size()-1; lengths[k]>nextSegSize; --k)
            { if(k==0) {k=-1;  break;} }
        //k=-1 means not found --> all lengths fit into the same segment size
        int d = lengths.size()-1-k;//distance between the last element and the index k found

        //Now we need to try to fill up all segments at least once; since
        //we make at least one transfer to M2i memory, we might as well use all segment
        //and we know that anything fitting the last run length will fit all others
        //IMPORTANT: WE HAVE TO USE AT MOST maxDiv-1 SEGMENTS TO LEAVE SPACE FOR AN END SEGMENT

        if (d<=(maxDiv-1))
        {
            uint_triplet tri {(uint32)max(0,(int)lengths.size()-(int)(maxDiv-1)), (uint32)lengths.size()-1U, maxDiv};
            res.push_back(tri);
            lengths.resize(max(0,(int)lengths.size()-(int)(maxDiv-1)));
        }
        else
        {
            if(d%(maxDiv-1)!=0) //if the number of lengths fitting for a divider of maxDiv is not a multiple of
                            //of the maxDiv (some segments at the end will have to stay empty)
            {
                uint_triplet tri {(uint32)(lengths.size()-d%(maxDiv-1)), (uint32)(lengths.size()-1), maxDiv};
                res.push_back(tri);
            }
            for(int i=d/maxDiv-1; i>=0; --i)
            {
                uint_triplet tri {k+1+i*(maxDiv-1), k+(i+1)*(maxDiv-1), maxDiv};
                res.push_back(tri);
            }

            lengths.resize(k+1);//remove all elements we just took care of!
        }
    }

    return res;
}



M2iCard::M2iCard():
    qwMemInBytes(0LL)
  , pvBuffer(nullptr)
{
}

M2iCard::~M2iCard()
{
    M2iClose();
    std::cout << "M2i-7005 Digital I/O PCIe card communication has been closed." << std::endl;
}


void M2iCard::M2iInit()
{
    // ------------------------------------------------------------------------
    // initialize card number 0 (the first card in the system), get some information and print it
    if (bSpcMInitCardByIdx(&stCard, 0))
    {
        //printf (pszSpcMPrintDocumentationLink (&stCard, szBuffer, sizeof (szBuffer)));
        printf (pszSpcMPrintCardInfo (&stCard, szBuffer, sizeof(szBuffer)));
        std::cout << std::flush;
    }
    else
    {
        nSpcMErrorMessageStdOut (&stCard, "Error: Could not open connection with M2i.7005exp DIO card\n", true);
        std::cout << std::flush;
    }
    std::cout << "Initialising M2i-7005 Digital I/O PCIe card communication" << std::endl;
}

//available channel enable mask in 1,2,4,8,16=all
//IMPORTANT : the memory size is given in samples = time points
bool M2iCard::M2iSetup_StdReplay(int64 chanMask,
                                 int64 clockSpeed,
                                 bool clockOut,
                                 bool triggerOut,
                                 long long nbLoops,
                                 unsigned int memSize
                                 )
{
    //configure clock speed
    bSpcMSetupClockPLL (&stCard, MEGA(clockSpeed), clockOut);
    printf ("Sampling rate set to %.1lf MHz\n", (double) stCard.llSetSamplerate / 1000000);
    std::cout << std::flush;
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut (&stCard, "Clock PLL setup error\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //set the number of activated channels, the memory size, the number of loops
    bSpcMSetupModeRepStdLoops(&stCard, chanMask, memSize, nbLoops);
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut(&stCard, "Replay mode setup error\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //set the trigger mode
    bSpcMSetupTrigSoftware(&stCard, triggerOut);
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut(&stCard, "Trigger setup error\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //set digital output voltage level
    for (int i=0; i < stCard.uCfg.stDIO.lGroups; i++)
        bSpcMSetupDigitalOutput(&stCard, i, SPCM_STOPLVL_LOW, 0, 3300);
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut(&stCard, "Output level setup error:\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    printf("Number of enabled channels is: %d\n", stCard.lSetChannels);
    printf("Replay memory size: %d\n", stCard.llSetMemsize);
    std::cout << std::flush;
    return true;
}





/*
By chunk we mean a set of repeated runs corresponding to a set of parameters of the sequence;
the memory can be divided into maximum 256 segments, a sequence needs one End segment;
if we want to measure more than 255 different parameters (for instance, 255 excitation times for a Rabi exp)
or at least one chunk exceeds the size of fullMemory/256, then we will have to divide the full
set of parameters into chunks
the number of segments the memory is divided into
actually depends on the length of the longest run in the chunk, which in turn
depends on the chunck the runs making up this chunk
i.e. for a linear range from 0 to 1ms (with 5us increment), the first runs
close to 0 are way faster than the ones at the end so you can pack more into a chunk
*/
bool M2iCard::M2iExpSequenceReplay(int chanMask,
                                   int64 clockSpeed,
                                   const std::vector<uint32>& runLengths,
                                   int loops,
                                   std::vector< multichanEdgeData >& risEdg,
                                   std::vector< multichanEdgeData >& fallEdg,
                                   bool clockOut,
                                   bool triggerOut
                                   )

{
    //check input data
    if(runLengths.size() != risEdg.size() || risEdg.size() != fallEdg.size())
    {
        std::cout << "Error detected in the size of input vector of run length and/or rising edges an/or falling edges. " << std::endl;
        return false;
    }
    //=============================================================================
    //                         GENERAL CONFIG
    //=============================================================================
    //configure clock speed
    bSpcMSetupClockPLL (&stCard, MEGA(clockSpeed), clockOut);
    printf ("Sampling rate set to %.1lf MHz\n", (double) stCard.llSetSamplerate / 1000000);
    std::cout << std::flush;
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut (&stCard, "Clock PLL setup error\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //set the trigger mode
    bSpcMSetupTrigSoftware(&stCard, triggerOut);
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut(&stCard, "Trigger setup error\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //set digital output voltage level
    for (int i=0; i < stCard.uCfg.stDIO.lGroups; i++)
        bSpcMSetupDigitalOutput(&stCard, i, SPCM_STOPLVL_LOW, 0, 3300);
    if (stCard.bSetError)
    {
        nSpcMErrorMessageStdOut(&stCard, "Output level setup error:\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //=============================================================================
    //             COMPUTE HOW MEMORY WILL BE DIVIDED TO FIT ALL RUNS
    //=============================================================================

    std::vector<uint_triplet> runChunks = chunks_from_length_set(runLengths);
    std::reverse(runChunks.begin(),runChunks.end());

    //=============================================================================
    //          NOW LOOP OVER EVERY CHUNK; INSIDE EACH CHUNK, EVERY RUN
    //            HAS TO BE PREPARED IN MEMORY AND IN SEQUENCE TABLE
    //=============================================================================

    //buffer for data transfer, containing multiplexed data later on
    int16* pDoubleByteBuffer;

    long step = 0;
    long long segment = 0LL;
    long long loopNb = 0LL;
    long long next = 0LL;
    long long condition = 0LL;
    long long paramWord = 0LL;

    for(int d=0; d<runChunks.size(); ++d)//loop over the chunks made of one or more runs
    {
        if (runChunks[d].el3==1)//DIVIDER IS ONE: NO NEED TO SEQUENCE THE MEMORY
                                // --> WE USE STANDARD REPLAY
        {
            //set the number of activated channels, the memory size, the number of loops
            bSpcMSetupModeRepStdLoops(&stCard, chanMask, runLengths[runChunks[d].el1], loops);
            if (stCard.bSetError)
            {
                nSpcMErrorMessageStdOut(&stCard, "Replay mode setup error\nExiting driver...\n", true);
                std::cout << std::flush;
                return false;
            }

            //allocate buffer for data transfer, containing multiplexed data later on
            qwMemInBytes = stCard.llSetMemsize * stCard.lSetChannels / 8;
            pvBuffer = pvAllocMemPageAligned (qwMemInBytes);
            if (!pvBuffer)
            {
                nSpcMErrorMessageStdOut (&stCard, "Memory allocation error!\nExiting driver...\n", true);
                std::cout << std::flush;
                return false;
            }
            int16* pDoubleByteBuffer = (int16*) pvBuffer;

            //translate and load the corresponding data into the buffer
            if (chanMask == (int64)0xFF)//8channels so nb of bytes = nb of samples
                translateTimestampsToParallelAndPutInBuffer(stCard.llSetMemsize, 8, risEdg[runChunks[d].el1], fallEdg[runChunks[d].el1], pDoubleByteBuffer);
            else
            {
                std::cout << "ERROR: Only planned for exactly 8 channels so far!" << std::endl;
                return false;
            }


            // we define the buffer for transfer and start the DMA transfer
            spcm_dwDefTransfer_i64 (stCard.hDrv, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, 0, pvBuffer, 0, qwMemInBytes);
            spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA);
            // check for error code after transfer
            if (spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError))
            {
                vFreeMemPageAligned (pvBuffer, qwMemInBytes);
                nSpcMErrorMessageStdOut (&stCard, "Error detected after attempted DMA transfer\nExiting driver...\n", true);
                std::cout << std::flush;
                return false;
            }

            vFreeMemPageAligned(pvBuffer, qwMemInBytes);

            //output data !
            if (spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_START | M2CMD_CARD_WAITREADY) != ERR_OK) // use this set of command if you want the function to return only when the DIO replay is finished
           {
               spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_STOP);
               spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError);
               nSpcMErrorMessageStdOut (&stCard, "Error detected after attempting output\nExiting driver...\n", true);
               std::cout << std::flush;
               return false;
           }

        }
        else
        {
            // ############# 1. set the right number of segments
            //following function does reset the card mode to "Sequence Replay" every time but it can't hurt
            bSpcMSetupModeRepSequence(&stCard, chanMask, runChunks[d].el3);
            if (stCard.bSetError)
            {
                nSpcMErrorMessageStdOut(&stCard, "Sequence replay mode setup error\nExiting driver...\n", true);
                std::cout << std::flush;
                return false;
            }

            int cStart = runChunks[d].el1;
            int cEnd = runChunks[d].el2;
            // ############# 2.for each run inside a chunk:
            //index in M2i card seq table goes from 0 to runChunks[d].el2-runChunks[d].el1
            //index for the run in the multichanEdgeData and the vector of lengths goes from runChunks[d].el1 to runChunks[d].el2
            for (int R=0; R<=cEnd-cStart; ++R)
            {
                // a. set the segment index on which to next write
                spcm_dwSetParam_i32(stCard.hDrv, SPC_SEQMODE_WRITESEGMENT, R);

                // b. set the right segment replay size = run length (contained in runLengths)
                // +++++++ 8channels: min 48 samples, step 16 +++++++
                // +++++++ 16channels: min 32 samples, step 8 +++++++
                spcm_dwSetParam_i32(stCard.hDrv, SPC_SEQMODE_SEGMENTSIZE, runLengths[R+cStart]);//segment size is in samples

                // c. allocate some memory and copy buffer address
                int lenInBytes;
                if (chanMask == (int64)0xFF)//8channels so nb of bytes = nb of samples
                    lenInBytes = runLengths[R+cStart];
                else
                {
                    std::cout << "ERROR: Only planned for exactly 8 channels so far!" << std::endl;
                    return false;
                }
                pvBuffer = pvAllocMemPageAligned (lenInBytes);//total size to allocate in bytes
                if (!pvBuffer)
                {
                    nSpcMErrorMessageStdOut (&stCard, "Memory allocation error!\nExiting driver...\n", true);
                    std::cout << std::flush;
                    return false;
                }
                pDoubleByteBuffer = (int16*) pvBuffer;

                // d. translate rising&falling edges info into parallel data and put in buffer
                if (chanMask == (int64)0xFF)//8channels so nb of bytes = nb of samples
                {
                    int chNb = 8;
                    translateTimestampsToParallelAndPutInBuffer((int)runLengths[R+cStart], chNb, risEdg[R+cStart], fallEdg[R+cStart], pDoubleByteBuffer);
                }
                else
                {
                    std::cout << "ERROR: Only planned for exactly 8 channels so far!" << std::endl;
                    return false;
                }

                // e. define and start data transfer into the card
                spcm_dwDefTransfer_i64 (stCard.hDrv, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, 0, pvBuffer, 0, lenInBytes);//size of transfer in bytes
                spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA);

                // f. set the sequence table data for the segment
                step = R;
                segment = R;
                loopNb = loops;
                condition = SPCSEQ_ENDLOOPALWAYS;
                next = R+1;

                paramWord = (condition<<32) | (loopNb<<32) | (next<<16) | (segment);

                if(spcm_dwSetParam_i64(stCard.hDrv, SPC_SEQMODE_STEPMEM0 + step, paramWord))
                {
                    vFreeMemPageAligned(pvBuffer, lenInBytes);
                    spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_STOP);
                    spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError);
                    nSpcMErrorMessageStdOut (&stCard, "Error detected after setting segment parameters for segment\nExiting driver...\n", true);
                    std::cout << std::flush;
                    return false;
                }

                // g. free allocated memory
                vFreeMemPageAligned(pvBuffer, lenInBytes);//total size to free in bytes
            }

            // ############# 3. add the END segment

            //the size of 2^21 for the end segment corresponds to the segment size for
            //the maximum number of memory segments i.e. 256, which is the minimum segment size
            //so 2^21 will fit for any number of segments

            spcm_dwSetParam_i32(stCard.hDrv, SPC_SEQMODE_WRITESEGMENT, cEnd - cStart +1);

            spcm_dwSetParam_i32(stCard.hDrv, SPC_SEQMODE_SEGMENTSIZE, 1L<<21);

            //allocate buffer for data transfer, containing multiplexed data later on
            pvBuffer = pvAllocMemPageAligned (1ULL<<21);
            if (!pvBuffer)
            {
                nSpcMErrorMessageStdOut (&stCard, "Memory allocation error!\nExiting driver...\n", true);
                std::cout << std::flush;
                return false;
            }
            pDoubleByteBuffer = (int16*) pvBuffer;

            //we put some zeros into the buffer
            for(int i = 0; i<(1<<20); ++i)
            {
                *pDoubleByteBuffer++ = (int16)0;
            }

            //define and then start the data transfer into the card
            spcm_dwDefTransfer_i64 (stCard.hDrv, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, 0, pvBuffer, 0, 1<<21);
            spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA);

            step = cEnd - cStart +1;
            segment = cEnd - cStart +1;
            loopNb = 1LL;
            condition = SPCSEQ_END;
            next = cEnd - cStart +1;

            paramWord = (condition<<32) | (loopNb<<32) | (next<<16) | (segment);

            if(spcm_dwSetParam_i64(stCard.hDrv, SPC_SEQMODE_STEPMEM0 + step, paramWord))
            {
                spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_STOP);
                spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError);
                nSpcMErrorMessageStdOut (&stCard, "Error detected after setting segment parameters for segment\nExiting driver...\n", true);
                std::cout << std::flush;
                return false;
            }

            vFreeMemPageAligned(pvBuffer, 1ULL<<21);

            // ############# 4. Then start the replay i.e. start output

            if (spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_START | M2CMD_CARD_WAITREADY) != ERR_OK) // use this set of command if you want the function to return only when the DIO replay is finished
            //if (spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_START) != ERR_OK)
            {
                spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_STOP);
                spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError);
                nSpcMErrorMessageStdOut (&stCard, "Error detected after attempting output\nExiting driver...\n", true);
                std::cout << std::flush;
                return false;
            }
        }
    }

    return true;
}


bool M2iCard::M2iSetConstLvlMode(int permLvlMode)
{
    switch(permLvlMode)
    {
        case PERMLVL_ALL_ZEROS:
            spcm_dwSetParam_i32 (stCard.hDrv, SPC_CH0_STOPLEVEL, SPCM_STOPLVL_LOW); break;
        case PERMLVL_ALL_ONES:
            spcm_dwSetParam_i32 (stCard.hDrv, SPC_CH0_STOPLEVEL, SPCM_STOPLVL_HIGH); break;
        case PERMLVL_INDIV:
            spcm_dwSetParam_i32 (stCard.hDrv, SPC_CH0_STOPLEVEL, SPCM_STOPLVL_HOLDLAST); break;
        case PERMLVL_HIGH_Z:
            spcm_dwSetParam_i32 (stCard.hDrv, SPC_CH0_STOPLEVEL, SPCM_STOPLVL_TRISTATE); break;
        default:
            printf("Non-valid definition for the stop level; check argument.\n");
            std::cout << std::flush;
            return false;
    }

    // check for error code
    if (spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError))
    {
        nSpcMErrorMessageStdOut (&stCard, "Error detected after defining constant level\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    return true;
}

bool M2iCard::M2iSetConstLvl(int actChanNb, uint16_t lvls)
{
    //the vector of constant 1s or 0s corresponding to what each channel should be set to
    std::vector<uint64_t> constLvlVec(actChanNb, 0ULL);
    for (int i=0; i<actChanNb; ++i)
    {
        if(lvls & (1<<i))
            constLvlVec[i] = 0xFFFFFFFFFFFFFFFF;
    }

    //---------------------------------

    //configure the system for the upload
    //we choose a sequence of 128samples because it is the smallest possible for the extreme case of only 1 channel activated
    //for 8 channels it is 16 samples minimum
    int64 channelMask = (1 << actChanNb) - 1;
    M2iSetup_StdReplay(channelMask, 10, false, false, 1, 128);
    //printf("Number of enabled channels is: %d\n", stCard.lSetChannels);
    //printf("Replay memory size: %d\n", stCard.llSetMemsize);
    std::cout << std::flush;

    //allocate buffer for data transfer, containing multiplexed data later on
    qwMemInBytes = 128 * stCard.lSetChannels / 8;
    pvBuffer = pvAllocMemPageAligned (qwMemInBytes);
    if (!pvBuffer)
    {
        nSpcMErrorMessageStdOut (&stCard, "Memory allocation error!\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    int16* pDoubleByteBuffer = (int16*) pvBuffer;

    //---------------------------------

    //parallelized the words (only 1s or only 0s) into int16s and put them in the allocated buffer
    //do it twice because the memory length is 128 and we work with 64bit words
    int m = 0;
    int16_t two_bytes;
    for (int K = 0; K<2; ++K)
    {
        for (int shift = 0; shift < 64; ++shift)
        {
            for(int aCh = 0; aCh < actChanNb; ++aCh)
            {
                if((uint64_t)(1ULL<<shift) & constLvlVec[aCh])
                    two_bytes += (int16_t)(1<<(m%16));
                if(m%16==15)
                {
                    //std::cout << "new int16: " << integ_to_binStr(two_bytes, true) << std::endl;
                    *pDoubleByteBuffer++ = two_bytes;
                    two_bytes = 0;
                }
                ++m;
            }
        }
    }

    //---------------------------------

    //define buffer for transfer and start DMA transfer (very short because only 128bits per channel!)
    spcm_dwDefTransfer_i64 (stCard.hDrv, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, 0, pvBuffer, 0, qwMemInBytes);
    spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA);
    // check for error code
    if (spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError))
    {
        vFreeMemPageAligned (pvBuffer, qwMemInBytes);
        nSpcMErrorMessageStdOut (&stCard, "Error detected after attempted DMA transfer\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    //---------------------------------

    //set the stop level mode to HOLDLAST
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // !!!!!!! WARNING: NEEDS TO BE DONE HERE AND NOT BEFORE BECAUSE OTHER COMMANDS SEEM TO OVERRIDE THIS SOMEHOW !!!!!!!
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    M2iSetConstLvlMode(PERMLVL_INDIV);

    //---------------------------------

    //makes sure that the stop mode was able to be set to HoldLast in order to individually define ch0,1,2,..7
    int64 stopLvlMode;
    if(spcm_dwGetParam_i64(stCard.hDrv, SPC_CH0_STOPLEVEL, &stopLvlMode)!=ERR_OK)
    {
        nSpcMErrorMessageStdOut (&stCard, "Error detected querying stop level mode\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    if (stopLvlMode!=SPCM_STOPLVL_HOLDLAST)
    {
        std::cout << "The stop level does not allow each digital channel/pin to have an independant constant state\nIt needs to be mode HOLDLAST" << std::endl;
        std::cout << std::flush;
        return false;
    }
    //---------------------------------

    //run the short sequence
    if (spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_START) != ERR_OK)
    {
        vFreeMemPageAligned (pvBuffer, qwMemInBytes);
        spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_STOP);
        spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError);
        nSpcMErrorMessageStdOut (&stCard, "Error detected after attempting output\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    //the final level of the sequence should be held by the bit/channel

    //---------------------------------

    //free allocated memory
    vFreeMemPageAligned (pvBuffer, qwMemInBytes);

    return true;
}



/*
bool M2iCard::M2iLoadIntoMemoryFromGui(int chanNumber)
{
    printf("Number of enabled channels is: %d\n", stCard.lSetChannels);
    printf("Replay memory size: %d\n", stCard.llSetMemsize);
    std::cout << std::flush;

    //allocate buffer for data transfer, containing multiplexed data later on
    qwMemInBytes = stCard.llSetMemsize * stCard.lSetChannels / 8;
    void* pvBuffer;
    pvBuffer = pvAllocMemPageAligned (qwMemInBytes);
    if (!pvBuffer)
    {
        nSpcMErrorMessageStdOut (&stCard, "Memory allocation error!\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    int16* pDoubleByteBuffer = (int16*) pvBuffer;

    //get the timestamps of edges from the GUI/Sequence Editor
    double s = ui->chan0->step;
    multichanEdgeData rising;
    multichanEdgeData falling;
    for(int j = 0; j<chanNumber; ++j)
    {
        rising.push_back(timestampsVec_doubleToInt(seqEditors_in_GUI[j]->seqData->risingEdges, s));
        falling.push_back(timestampsVec_doubleToInt(seqEditors_in_GUI[j]->seqData->fallingEdges, s));
    }

    //translate and load the corresponding data into the buffer
    translateTimestampsToParallelAndPutInBuffer(stCard.llSetMemsize, chanNumber, rising, falling, pDoubleByteBuffer);

    // we define the buffer for transfer and start the DMA transfer
    printf ("Starting the DMA transfer and waiting until data is in board memory\n");
    std::cout << std::flush;
    spcm_dwDefTransfer_i64 (stCard.hDrv, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, 0, pvBuffer, 0, qwMemInBytes);
    spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA);

    // check for error code after transfer
    if (spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError))
    {
        vFreeMemPageAligned (pvBuffer, qwMemInBytes);
        nSpcMErrorMessageStdOut (&stCard, "Error detected after attempted DMA transfer\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    vFreeMemPageAligned(pvBuffer, qwMemInBytes);
    std::cout << "End the DMA transfer\n" << std::endl;
    return true;
}
*/


bool M2iCard::M2iLoadIntoMemoryFromVec(int chanNumber, multichanEdgeData rising, multichanEdgeData falling)
{
    printf("Number of enabled channels is: %d\n", stCard.lSetChannels);
    printf("Replay memory size: %d\n", stCard.llSetMemsize);
    std::cout << std::flush;

    //allocate buffer for data transfer, containing multiplexed data later on
    qwMemInBytes = stCard.llSetMemsize * stCard.lSetChannels / 8;
    void* pvBuffer;
    pvBuffer = pvAllocMemPageAligned (qwMemInBytes);
    if (!pvBuffer)
    {
        nSpcMErrorMessageStdOut (&stCard, "Memory allocation error!\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    int16* pDoubleByteBuffer = (int16*) pvBuffer;

    //translate and load the corresponding data into the buffer
    translateTimestampsToParallelAndPutInBuffer(stCard.llSetMemsize, chanNumber, rising, falling, pDoubleByteBuffer);

    // we define the buffer for transfer and start the DMA transfer
    printf ("Starting the DMA transfer and waiting until data is in board memory\n");
    std::cout << std::flush;
    spcm_dwDefTransfer_i64 (stCard.hDrv, SPCM_BUF_DATA, SPCM_DIR_PCTOCARD, 0, pvBuffer, 0, qwMemInBytes);
    spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_DATA_STARTDMA | M2CMD_DATA_WAITDMA);

    // check for error code after transfer
    if (spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError))
    {
        vFreeMemPageAligned (pvBuffer, qwMemInBytes);
        nSpcMErrorMessageStdOut (&stCard, "Error detected after attempted DMA transfer\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }

    vFreeMemPageAligned(pvBuffer, qwMemInBytes);
    std::cout << "End the DMA transfer\n" << std::endl;
    return true;
}


bool M2iCard::M2iOutputData()
{
    //send command to start outputting data
    printf("Data output...\n");
    std::cout << std::flush;
    if (spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_START | M2CMD_CARD_WAITREADY) != ERR_OK) // use this set of command if you want the function to return only when the DIO replay is finished
    //if (spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_START) != ERR_OK)
    {
        spcm_dwSetParam_i32 (stCard.hDrv, SPC_M2CMD, M2CMD_CARD_STOP);
        spcm_dwGetErrorInfo_i32 (stCard.hDrv, NULL, NULL, stCard.szError);
        nSpcMErrorMessageStdOut (&stCard, "Error detected after attempting output\nExiting driver...\n", true);
        std::cout << std::flush;
        return false;
    }
    std::cout << "DATA OUTPUT COMPLETE !" << std::endl;
    return true;
}

void M2iCard::M2iClose()
{
    // clean up and close the driver
    printf("Closing driver...\n");
    std::cout << std::flush;
    vSpcMCloseCard (&stCard);
    //vFreeMemPageAligned (pvBuffer, qwMemInBytes);
}
