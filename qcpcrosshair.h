#ifndef QCPCROSSHAIR_H
#define QCPCROSSHAIR_H
#include "qcustomplot.h"
#include "qcpmovablestraightline.h"
#include <iostream>


class QCPCrosshair
{

public:
    QCPCrosshair(QCustomPlotWithROI* parentPlot);

    //QCPItemTracer intersection;
    QCPMovableStraightLine lineVertical;
    QCPMovableStraightLine lineHorizontal;

};

#endif // QCPCROSSHAIR_H
