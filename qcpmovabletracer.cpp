#include "qcpmovabletracer.h"

QCPMovableTracer::QCPMovableTracer(QCustomPlotWithROI* parentPlot):QCPItemTracer(parentPlot)
{
    position_startOfMove.setX(0);
    position_startOfMove.setY(0);
}


void QCPMovableTracer::mouseMoveEvent(QMouseEvent* event, const QPointF& startPos)
{

}


void QCPMovableTracer::mousePressEvent(QMouseEvent* event, const QVariant& details)
{
    if(event->button()==Qt::LeftButton)
    {
        position_startOfMove.setX(position->coords().x());
        position_startOfMove.setY(position->coords().y());
        flagMoving = true;
    }
}


void QCPMovableTracer::mouseReleaseEvent(QMouseEvent* event, const QPointF &startPos)
{
    if(event->button()==Qt::LeftButton)
        flagMoving = false;
}
