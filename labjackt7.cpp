#include "libs/LabJack/LJM_Utilities.h"
#include "libs/LabJack/LabJackM.h"
#include <iostream>
#include "labjackt7.h"
#include <cstring>


LabJackT7::LabJackT7(): LJError(0), T7Handle(0)
{
    std::cout << "Opening LabJack driver." << std::endl;
    LJError = LJM_Open(LJM_dtT7, LJM_ctTCP, IP_addr_T7, &T7Handle);
    ErrorCheck(LJError, "Error on LJM_Open");
    std::cout << "T7 module accessible\n" << std::endl;
    //information about the device and its connection
    PrintDeviceInfoFromHandle(T7Handle);
    std::cout << std::flush;
}

LabJackT7::~LabJackT7()
{
    //Close LabJack T7 connection
    LJError = LJM_Close(T7Handle);
    std::cout << "Labjack driver closed.\nT7 module not available" << std::endl;
    ErrorCheck(LJError, "Error on LJM_Close");
}

double LabJackT7::analogReadOut(const char* channel)
{
    //read the current value from the analog channel
    double read_value = 0;
    LJError = LJM_eReadName(T7Handle, channel, &read_value);
    ErrorCheck(LJError, "Error on ReadValue");

    //tell the LCD display widget that a new value is ready IN THE CASE OF THE FAST CHANNEL ONLY
    if(strcmp(anChanFast1, channel)==0)
        emit(valueRead(read_value));
    
    return read_value;
}

//the limit check is redundant here since the limits are already set with the min and max of the QDoubleSpinBox
//but we keep the limits in this function out of carefulness in case it is used without the
//QDoubleSpinBox
void LabJackT7::extDAC_setVoltage(int DACchannel, double voltage, DACoutLimits lims)
{
    const char* name;
    switch(DACchannel)
    {
        case 0: name = DACch0; break;
        case 1: name = DACch1; break;
    }

    double limitedV = 0;
    if(voltage<lims.low_lim)
        limitedV = lims.low_lim;
    else if(voltage>lims.high_lim)
        limitedV = lims.high_lim;
    else
        limitedV = voltage;
    LJError = LJM_eWriteName(T7Handle, name, limitedV);

    ErrorCheck(LJError, "Error on setting extDAC value");
}


void LabJackT7::pulseSetupAndEnable(int pulseNb, int pulseLen, int totCycleLength)
{
    double dutyCycle = (double)pulseLen/(double)totCycleLength;
    emit(dutyCycleVal(dutyCycle));

    //============= IMPORTANT ======================
    // the name pulseDIO and the number # in.
    // DIO#_EF_.. below for the config of the pulse
    // out have to be the same !!!!!!!!!!!
    //==============================================
    //make sure DIO port is low and clock is disabled first

    LJError = LJM_eWriteName(T7Handle, pulseDIO, 0);
    const char* nameCL = "DIO_EF_CLOCK0_ENABLE";
    LJError = LJM_eWriteName(T7Handle, nameCL, 0);
    ErrorCheck(LJError, "Error on CLOCK0_ENABLE");

    //configure clock and DIO_EF (=Extended Features)
    //the DIO pin outputting the pulses is set here in the names
    const char* arNamesConfig[9] =
    {
        "DIO0_EF_INDEX",
        "DIO0_EF_OPTIONS",
        "DIO0_EF_CONFIG_A",
        "DIO0_EF_CONFIG_B",
        "DIO0_EF_CONFIG_C",
        "DIO_EF_CLOCK0_DIVISOR",
        "DIO_EF_CLOCK0_ROLL_VALUE",
        "DIO_EF_CLOCK0_ENABLE",
        "DIO0_EF_ENABLE"
    };

    if (totCycleLength<=pulseLen)
    {
        std::cout << "Cycle length cannot be smaller than pulse length\nIt is set to a default of 2x pulse length." << std::endl;
        totCycleLength = 2*pulseLen;
    }

    //############################
    //assuming 80MHz core clock,
    //clock for PulseOut is f=10MHz <=> T=100ns for a divisor of 8
    //############################
    double arValuesConfig[9] =
    {
        2,   //INDEX --> corresponds to Pulse Out
        0,   //OPTIONS --> Clock Id
        pulseLen*10.,       //CONFIG_A -> Hi to Lo point, multiply by 10 to have µs as units since T=100ns
        0,                  //CONFIG_B -> Lo to Hi point
        pulseNb*1.,    //CONFIG_C -> Number of pulses
        8,   //DIVISOR
        totCycleLength*10.,//ROLL_VALUE -> nb of clock cycles during which 1 pulse is output
        1,   // 1 to enable CLOCK#, 0 to disable CLOCK#
        1    //1 to enable DIO#, 0 to disable DIO#
    };



    int err_addr = 0;
    LJError = LJM_eWriteNames(T7Handle, 9, arNamesConfig, arValuesConfig, &err_addr);
    ErrorCheck(LJError, "Error on eWriteNames for PulseOut");
    ErrorCheckWithAddress(LJError, err_addr, "LJM_eWriteNames - arNamesConfig");
}

void LabJackT7::pulseDisable()
{
    int err_addr = 0;
    const char* arNamesDisable[2] = { "DIO_EF_CLOCK0_ENABLE", "DIO0_EF_ENABLE"};
    double arValuesDisable[2] = {0,0};
    LJError = LJM_eWriteNames(T7Handle, 2, arNamesDisable, arValuesDisable, &err_addr);
    ErrorCheckWithAddress(LJError, err_addr, "LJM_eWriteNames - arNamesDisable");
}
