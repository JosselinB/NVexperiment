#ifndef QSEQEDITOR_H
#define QSEQEDITOR_H
#include "qcustomplot.h"
#include "qcpsequencedata.h"


struct linspaced;


std::vector< std::vector<double> > recurs_replace(std::vector<linspaced> ranges, std::vector<double>& V);


class QSeqEditor : public QCustomPlot
{
    Q_OBJECT
public:
    QSeqEditor(QWidget* parentWidget);
    QCPSequenceData* seqData;
    double step, Tmax;

public slots:
    void printAfterRangeChanged(const QCPRange& range);
    void setNewRange(const QCPRange& range);
    void resetData(double step, double maxTime);

};

#endif // QSEQEDITOR_H
