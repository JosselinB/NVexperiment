
#ifndef LABJACKT7_H
#define LABJACKT7_H

#include <QObject>

#define anChanFast1 "AIN0"
#define anChanSlow1 "AIN2"
#define DACch0 "TDAC2" //used with the LabJack Tick DAC add-on
#define DACch1 "TDAC3" //used with the LabJack Tick DAC add-on
#define pulseDIO "FIO0"

static const char* IP_addr_T7 = "141.51.198.19";


typedef struct DACoutLimits
{
    double low_lim;
    double high_lim;

} DACoutLimits;

static const DACoutLimits VCA_RFSA2113_limits = {0,3.};
static const DACoutLimits maxLimits = {-10.,10.};

class LabJackT7 : public QObject
{
    Q_OBJECT
public:
    
    int T7Handle;
    int LJError;
    
    LabJackT7();
    ~LabJackT7();

    void pulseSetupAndEnable(int pulseNb, int pulseLen, int totCycleLength);
    void pulseDisable();
    void extDAC_setVoltage(int DACchannel, double V, DACoutLimits lims);
    
    double analogReadOut(const char* channel);
    
signals:
    void valueRead(double d);
    void dutyCycleVal(double d);
    
};

#endif // LABJACKT7_H
