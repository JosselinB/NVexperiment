#include "trigsequencegen.h"
#include <cmath>
#include <algorithm>
#include <iostream>

//all times here in ns
static const float delayLaserEnableRise = 300.;
static const float delayLaserEnableFall = 200.;
static const float delayLaserDigitalMod = 20.;
static const float delayM8195ATriggerIn = 618. ;
static const float delayM8195AEventIn = 618. ;
static const float laserPrepareTime = 1500.;
static const float offsetStart = 10.;
static const float pauseAfterPrepare = 1200.;

static const float M8195AEventLength =  50.;
static const float M8195ATriggerLength = 50.;



trigSequenceGen::trigSequenceGen():channelNb(8), timeStep_DIO_in_ns(10)
{
    fallingEdges.resize(channelNb);
    risingEdges.resize(channelNb);
}

void trigSequenceGen::eraseAllEdges()
{
    for (int i=0; i<channelNb; ++i)
    {
        risingEdges[i].resize(0);
        fallingEdges[i].resize(0);
    }
}

int trigSequenceGen::ns_to_steps(double t)
{
    return std::lround(t/timeStep_DIO_in_ns);
}

void trigSequenceGen::adjust_first_rising(int offset)
{
    std::vector<int> firsts;
    for(int i=0; i<channelNb; ++i)
    {
        if(risingEdges[i].size()>0)
        {
            std::sort(risingEdges[i].begin(), risingEdges[i].end());
            firsts.push_back(risingEdges[i][0]);
        }
    }

    int M = *std::min_element(firsts.begin(), firsts.end());
    int Delta = offset - M;

    for(int j=0;j<channelNb; ++j)
    {
        for(int a=0; a<risingEdges[j].size(); ++a)
            {risingEdges[j][a] += Delta;}
        for(int b=0; b<fallingEdges[j].size(); ++b)
            {fallingEdges[j][b] += Delta;}
    }
}


//make a special function to append a laser pulse because it involves
//actually making 2 pulses, one on laser enable input, one on
//digital modulation input
void trigSequenceGen::appendLaserPulse(int start, int length)
{
    risingEdges[DIOchannel::laserEnable].push_back(ns_to_steps(start - delayLaserEnableRise));
    fallingEdges[DIOchannel::laserEnable].push_back(ns_to_steps(start + length - delayLaserEnableFall));

    risingEdges[DIOchannel::laserDigitalMod].push_back(ns_to_steps(start - delayLaserDigitalMod));
    fallingEdges[DIOchannel::laserDigitalMod].push_back(ns_to_steps(start + length - delayLaserDigitalMod));
}

void trigSequenceGen::CWODMR(double integTime)
{
    return;
}

void trigSequenceGen::pulsedODMR(double readOutTime, double piPulseTime)
{
    eraseAllEdges();

    appendLaserPulse(ns_to_steps(laserPrepareTime), ns_to_steps(offsetStart));//preparation pulse
    appendLaserPulse(ns_to_steps(readOutTime), ns_to_steps(offsetStart + laserPrepareTime + pauseAfterPrepare + piPulseTime + 30.));//readout pulse

    risingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart + laserPrepareTime + pauseAfterPrepare + piPulseTime + 30.));
    fallingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart + laserPrepareTime + pauseAfterPrepare + piPulseTime + 30. + readOutTime));

    risingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(offsetStart + laserPrepareTime + pauseAfterPrepare - delayM8195AEventIn));
    fallingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(offsetStart + laserPrepareTime + pauseAfterPrepare + M8195AEventLength - delayM8195AEventIn));

    adjust_first_rising(offsetStart);
}

void trigSequenceGen::RabiExp(double T, double readOutTime, bool ref, double piTime)
{
    eraseAllEdges();

    if(piTime>pauseAfterPrepare-100.)
    {
        std::cout << "The pi time is too long for the hardcoded pause after polarisation/state preparation! Please change either of them." << std::endl;
        return;
    }

    if(!ref)
    {
        //prepare pulse
        appendLaserPulse(ns_to_steps(laserPrepareTime), ns_to_steps(offsetStart));
        //readout pulse
        appendLaserPulse(ns_to_steps(readOutTime), ns_to_steps(offsetStart + laserPrepareTime + T));

        risingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart + laserPrepareTime + T));
        fallingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart + laserPrepareTime + T + readOutTime));

        risingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(offsetStart + laserPrepareTime - delayM8195AEventIn));
        fallingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(offsetStart + laserPrepareTime + M8195AEventLength - delayM8195AEventIn));
    }
    else
    {
        //============================ Pulses for dark/bright reference ============================
        appendLaserPulse(ns_to_steps(laserPrepareTime), ns_to_steps(offsetStart));//prepare for dark reference
        appendLaserPulse(ns_to_steps(laserPrepareTime), ns_to_steps(offsetStart+laserPrepareTime+pauseAfterPrepare));//prepare for bright ref & readout dark ref
        appendLaserPulse(ns_to_steps(laserPrepareTime), ns_to_steps(offsetStart+2*laserPrepareTime+2*pauseAfterPrepare));//readout for bright ref

        risingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart+laserPrepareTime+pauseAfterPrepare));
        fallingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart+laserPrepareTime+pauseAfterPrepare+readOutTime));
        risingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart+2*laserPrepareTime+2*pauseAfterPrepare));
        fallingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart+2*laserPrepareTime+2*pauseAfterPrepare+readOutTime));

        risingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(offsetStart+2*laserPrepareTime+pauseAfterPrepare+100. - delayM8195AEventIn));
        fallingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(offsetStart+2*laserPrepareTime+pauseAfterPrepare+100.+ M8195AEventLength - delayM8195AEventIn));
        //============================ End of pulses for reference ============================

        int endOfRef = offsetStart+3*laserPrepareTime+2*pauseAfterPrepare;

        //prepare pulse
        appendLaserPulse(ns_to_steps(laserPrepareTime), ns_to_steps(endOfRef+offsetStart));
        //readout pulse
        appendLaserPulse(ns_to_steps(readOutTime), ns_to_steps(endOfRef+offsetStart + laserPrepareTime + T));

        risingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(endOfRef+offsetStart + laserPrepareTime + T));
        fallingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(endOfRef+offsetStart + laserPrepareTime + T + readOutTime));

        risingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(endOfRef+offsetStart + laserPrepareTime - delayM8195AEventIn));
        fallingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(endOfRef+offsetStart + laserPrepareTime - delayM8195AEventIn + M8195AEventLength));
    }

    adjust_first_rising(offsetStart);

}


//implementing the wait between pi/2 pulses of the Ramsey scheme in samples of the AWG allows
//for much finer control on the timing resolution of this wait
void trigSequenceGen::RamseyExp_waitInAWG(double T, double piHalfTime, double readOutTime, bool ref, double piTime)
{

    eraseAllEdges();
    if(!ref)
    {
        //prepare pulse
        appendLaserPulse(ns_to_steps(laserPrepareTime), ns_to_steps(offsetStart));
        //readout pulse
        appendLaserPulse(ns_to_steps(readOutTime), ns_to_steps(offsetStart + laserPrepareTime + 2*piHalfTime + T));

        risingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart + laserPrepareTime + T + 2*piHalfTime));
        fallingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart + laserPrepareTime + T + 2*piHalfTime + readOutTime));

        risingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(offsetStart + laserPrepareTime - delayM8195AEventIn));
        fallingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(offsetStart + laserPrepareTime + M8195AEventLength - delayM8195AEventIn));
    }
    else
    {

        //============================ Pulses for dark/bright reference ============================
        appendLaserPulse(ns_to_steps(laserPrepareTime), ns_to_steps(offsetStart));//prepare for dark reference
        appendLaserPulse(ns_to_steps(laserPrepareTime), ns_to_steps(offsetStart+laserPrepareTime+pauseAfterPrepare));//prepare for bright ref & readout dark ref
        appendLaserPulse(ns_to_steps(laserPrepareTime), ns_to_steps(offsetStart+2*laserPrepareTime+2*pauseAfterPrepare));//readout for bright ref

        risingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart+laserPrepareTime+pauseAfterPrepare));
        fallingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart+laserPrepareTime+pauseAfterPrepare+readOutTime));
        risingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart+2*laserPrepareTime+2*pauseAfterPrepare));
        fallingEdges[DIOchannel::TimeTagger20].push_back(ns_to_steps(offsetStart+2*laserPrepareTime+2*pauseAfterPrepare+readOutTime));

        risingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(offsetStart+2*laserPrepareTime+pauseAfterPrepare+100. - delayM8195AEventIn));
        fallingEdges[DIOchannel::AWGEventIn].push_back(ns_to_steps(offsetStart+2*laserPrepareTime+pauseAfterPrepare+100.+ M8195AEventLength - delayM8195AEventIn));
        //============================ End of pulses for reference ============================

        int endOfRef = offsetStart+3*laserPrepareTime+2*pauseAfterPrepare;

    }

    adjust_first_rising(offsetStart);
}

void RamseyExp_waitInDIOcard(double T, double piHalfTime, double readOutTime, bool noRef)
{

}




