#ifndef M2ICARD_H
#define M2ICARD_H

#include <vector>
#include <filesystem>
#include <iostream>
#include <algorithm>
#include <string>
#include <cmath>

#include "qcpsequencedata.h"


// ----- include standard driver header from library -----
#include "libs/Spectrum_Instr/c_header/dlltyp.h"
#include "libs/Spectrum_Instr/c_header/regs.h"
#include "libs/Spectrum_Instr/c_header/spcerr.h"
#include "libs/Spectrum_Instr/c_header/spcm_drv.h"

// ----- include of common example librarys -----
#include "libs/Spectrum_Instr/common/spcm_lib_card.h"
#include "libs/Spectrum_Instr/common/spcm_lib_data.h"

// ----- operating system dependent functions for thread, event, keyboard and mutex handling -----
#include "libs/Spectrum_Instr/common/ostools/spcm_oswrap.h"
#include "libs/Spectrum_Instr/common/ostools/spcm_ostools.h"


#define PERMLVL_ALL_ZEROS 0
#define PERMLVL_ALL_ONES 1
#define PERMLVL_INDIV 2
#define PERMLVL_HIGH_Z 3

typedef struct uint_triplet {
    uint32 el1;
    uint32 el2;
    uint32 el3;
} uint_triplet;

//returns the next (integer) power of 2 after the parameter i.e. log2(x)+1 if x!=2^n, otherwise n
int next_power_of_2(int A);
void translateTimestampsToParallelAndPutInBuffer(int seqLength,
                                                 int chanNb,
                                                 std::vector< std::vector<int> >& timestamps_rise,
                                                 std::vector< std::vector<int> >& timestamps_fall,
                                                 int16_t* pBuffer
                                                 );
void chunks_from_length_set(std::vector<int> params);

class M2iCard
{
public:

    char                szBuffer[1024];     // a character buffer for any messages
    ST_SPCM_CARDINFO    stCard;             // info structure of my card
    uint64              qwMemInBytes;
    void*               pvBuffer;

    M2iCard();
    ~M2iCard();

    //Pulsed Tap
    void M2iInit();
    bool M2iSetup_StdReplay(int64 chanMask,
                            int64 clockSpeed,
                            bool clockOut,
                            bool triggerOut,
                            long long nbLoops,
                            unsigned int memSize
                            );
    bool M2iExpSequenceReplay(int chanMask,
                              int64 clockSpeed,
                              const std::vector<uint32>& runLengths,
                              int loops,
                              std::vector< std::vector< std::vector<int> > >& risEdg,
                              std::vector< std::vector< std::vector<int> > >& fallEdg,
                              bool clockOut,
                              bool triggerOut
                              );

    //void* generateSeqData(std::vector<void*> edges);
    bool M2iLoadIntoMemoryFromGui(int chanNumber);
    bool M2iLoadIntoMemoryFromVec(int chanNumber, std::vector< std::vector<int> > rising, std::vector< std::vector<int> > falling);
    bool M2iOutputData();
    bool M2iSetConstLvlMode(int perm_mode);
    bool M2iSetConstLvl(int actChanNb, uint16_t lvls);
    void M2iClose();

    bool M2iLoadExpSequenceReplay(int chanMask, int64 clockSpeed, std::vector<uint32> runLengths, bool clockOut, bool triggerOut);

    bool writeSeqToFile(int nbActChan,
                        int length,
                        double tStep_in_ns,
                        const std::vector< std::vector<edgeProp> >& risingE_Vec,
                        const std::vector< std::vector<edgeProp> >& fallingE_Vec,
                        const std::string& path,
                        const std::string& name
                        );
    bool loadSeqFromFile(std::filesystem::path filePath,
                         std::string filename,
                         std::vector< std::vector<edgeProp> >& risingE_outVec,
                         std::vector< std::vector<edgeProp> >& fallingE_outVec
                         );
};

#endif // M2ICARD_H
