#ifndef SCANANDCOUNT_H
#define SCANANDCOUNT_H

#include <QMainWindow>



#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <QMessageBox>
#include <QTimer>
#include <utility>
#include <filesystem>
#include <direct.h>

static const long initValueWaveTableRate = 600;
static const double servoCycleTimeInMillisec = .05;
static const double minTimePerPtInMillisec = 20.;
static const int minWaveTableRate = lround(minTimePerPtInMillisec/servoCycleTimeInMillisec);



template<typename T>
T Min(T t1, T t2)
{
    if (t1<t2)
        return t1;
    else
        return t2;
}


//needs the dummy so that X,Y,Z corresponds to 1,2,3 and not 0,1,2
//if int is need, cast using static_cast<int>(a)
enum class Axis
{
    dummy_offset,
    X,
    Y,
    Z
};

//turns an axis from the enum to the corresponding string
/*QString AxisToQString(Axis A)
{
    QString s;

    switch(A)
    {
    case Axis::X : s = "X"; break;
    case Axis::Y : s = "Y"; break;
    case Axis::Z : s = "Z"; break;
    }
    return s;
}*/


struct Pos2D
{
    int X;
    int Y;
};
typedef Pos2D Pos2D;

typedef struct scanParams
{
    int Xstep;
    int Ystep;
    int Npx_X;
    int Npx_Y;
    Pos2D center;
    int Z;
    int Npx_Z;
    int Zstep;
    int MosaicLeft;
    int MosaicRight;
    int MosaicUp;
    int MosaicDown;

} scanParams;

//class TimeTagger;
class QCustomPlotWithROI;
class TimeTagger;
class Countrate;
class CountBetweenMarkers;

class ScanAndCount : public QObject
{
    Q_OBJECT
public:
    ScanAndCount(QCustomPlotWithROI* plot, bool E725_connected, bool TT20_connected);
    ~ScanAndCount();

    QCustomPlotWithROI* image2DPlot;
    std::vector<double> imageVec; //pixel values in Count or kCount per second
    std::string currentImageDate_yyyymmddHHMMSS;
    std::string currentImagePath;
    bool E725_physConn;
    bool TT20_physConn;

    scanParams currentScanParameters;

    int ID; //for the connection to E-725 controller
    TimeTagger* TT; // for connection to TimeTagger20
    //

    //for error message handling from E-725 piezo controller
    char szErrorMessage[1024];
    int	iError;

    //char bufferDataRec[600];

    bool scanningOn;
    bool scanningStoppedByCommand;

    std::filesystem::path globalPath;

    Countrate* countRate;

public slots:
    int getIStepXmax();
    int getIStepYmax();
    bool setupWaveTableScan_ZigZag(int Lx, int Ly, int Xstep, int Ystep);
    //bool setupWaveTableScan_Spiral(size_t Lx, size_t Ly);
    Pos2D waveGenOffsetForZigZag(int Lx, int Ly, int Xstep, int Ystep, Pos2D pt);
    bool startWaveGenWithCounterScan_ZigZag(int Lx, int Ly, int Xstep, int Ystep, Pos2D center);
    bool setWaveTableRate(int WTRvalue);

    bool moveAxisTo_SmallSteps(Axis A, int target);
    bool moveAxisTo_VelLimit(Axis A, int target);
    bool moveAxisStep_VelLimit(Axis A, int step);
    bool setServo(Axis A, bool b);
    bool stopMove();

    bool setOpenLoopValue(Axis A, double value);
    bool readAllPiezoVoltages();
    std::vector<double> readAllPositions_Closed();
    std::vector<double> readAllPositions_Open();
    //### std::vector<double> getRecordedData(int length);


    void fetchAndProcessData(CountBetweenMarkers& onTargetWindowsWPhotons, CountBetweenMarkers& pixelMarkers, bool print, bool fullScan, std::string scanType);
    void writeDataToFile(std::vector<int> counts, std::vector<double> bins, std::string ScanType, std::string date);
    void plotData(int Lx, int NumbersOfPixels);

    // Functions to comunicate with Mainwindow
    void on_TwoDScanStart_clicked_scanandcount();
    void on_testButton_clicked_scanandcount();
    void on_TwoDScanSetup_clicked_scanandcount(int Lx, int Ly, int Xstep, int Ystep);
    void on_STOPButton_clicked_scanandcount();
    void on_loopAXIS_stateChanged(int state, Axis AX);
    void on_ThreeDScanStart_clicked_scanandcount();
    void set3DScanParameters(int Lz, int Zstep);
    void setGlobalPath(std::filesystem::path globPath);
    void bleaching(int xSteps, int ySteps, int time);
    void setMosaicParameters(int LeftPicturesMosaic, int RightPicturesMosaic, int UpPicturesMosaic, int DownPicturesMosaic);
    void StartMosaicScan();
    double getCurrentCountsFromTT();

signals:
    void scanProgress_pxCount(int val);
    void readyFor2DPlot();
    void ScanPlot2D(int Lx, int NumbersOfPixels);
};

#endif // SCANANDCOUNT_H
