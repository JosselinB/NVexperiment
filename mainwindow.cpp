#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include <sstream>

#include "libs/PI/include/PI_GCS2_DLL.h"
#include "loadfilecreatmosaic.h"
#include "scanandcount.h"
#include "m2icard.h"
#include "m8195adev.h"
#include "alglib/fittingroutines.h"
#include "trigsequencegen.h"
#include "labjackt7.h"



#define NO_DEVICE_PHYS_CONNECT false

#define M8195A_PHYS_CONNECT (false&&(!NO_DEVICE_PHYS_CONNECT))
#define TT20_PHYS_CONNECT (false&&(!NO_DEVICE_PHYS_CONNECT))
#define PI_E725_PHYS_CONNECT (false&&(!NO_DEVICE_PHYS_CONNECT))
#define M2iCARD_PHYS_CONNECT (false&&(!NO_DEVICE_PHYS_CONNECT))
#define MAG_MOTORS_PHYS_CONNECT (false&&(!NO_DEVICE_PHYS_CONNECT))
#define LJ_T7_PHYS_CONNECT (true&&(!NO_DEVICE_PHYS_CONNECT))

std::string getNow()
{
    auto now = std::chrono::system_clock::now();
    auto nowT = std::chrono::system_clock::to_time_t(now);
    //use a C-string here because it was impossible to find how to use a
    //C++-style of time formatting and strftime works and is pretty convenient
    char strNow[20];
    strftime(strNow, 20, "%F %T", localtime(&nowT));
    std::string stdStrNow{ strNow };//make std::string out of C-string

    return stdStrNow;
}



// set the color gradient to custom gradient:
QCPColorGradient rainBowPlusWhiteGradient;
void setGradient_rainbowPlusWhite()
{
    rainBowPlusWhiteGradient.clearColorStops();
    rainBowPlusWhiteGradient.setColorStopAt(0, QColor(0, 0, 0));
    rainBowPlusWhiteGradient.setColorStopAt(0.25, QColor(0, 0, 255));
    rainBowPlusWhiteGradient.setColorStopAt(0.40, QColor(50, 180, 50));
    rainBowPlusWhiteGradient.setColorStopAt(0.50, QColor(255, 255, 0));
    rainBowPlusWhiteGradient.setColorStopAt(0.75, QColor(255, 0, 0));
    rainBowPlusWhiteGradient.setColorStopAt(1, QColor(255, 255, 255));
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , M2i7005_var(nullptr)
    , ScanAndCount_var(nullptr)
    , LJ_T7(nullptr)
    , acquisitionTimerFast(new QTimer()), acquisitionTimerSlow(new QTimer())
    , data_length_LJ(500)
{
    ui->setupUi(this);

    //define own color gradient for color map
    setGradient_rainbowPlusWhite();

    LoadFileCreatMosaic_var = new LoadFileCreatMosaic(ui->QCPimageWROI, ui->image3DPlot);
    colorScale3Dplot = new QCPColorScale(ui->image3DPlot);
    colorScale2Dplot = new QCPColorScale(ui->QCPimageWROI);

    if (M8195A_PHYS_CONNECT)
    {   M8195A_var = new M8195Adev;
        // open M8195A driver
        M8195A_var->setTriggermode("Triggered");
    }
    if (M2iCARD_PHYS_CONNECT)
    {   M2i7005_var = new M2iCard;
        // open M2i card driver
        M2i7005_var->M2iInit();
    }


    if(PI_E725_PHYS_CONNECT || TT20_PHYS_CONNECT)
    {
        ScanAndCount_var = new ScanAndCount(ui->QCPimageWROI, PI_E725_PHYS_CONNECT, TT20_PHYS_CONNECT);
    }

    if(ScanAndCount_var)
    {
        connect(ScanAndCount_var, SIGNAL(scanProgress_pxCount(int)), ui->scanProgress, SLOT(setValue(int)));//for progress bar display
        connect(ScanAndCount_var, SIGNAL(readyFor2DPlot()), this, SLOT(plotAfterDataAcq()));//for plotting data after acquisition
        connect(ScanAndCount_var, SIGNAL(ScanPlot2D(int,int)), this, SLOT(plot2DData(int, int)));//for plotting the 2D data
        connect(ui->QCPimageWROI, SIGNAL(posMovedTo(double, double)), this, SLOT(moveToPosition(double, double)));

        //create directory for current day (if it does not exist) and sets the member variable 'globalPath' to the full name of this directory
        //for writing data and images into this directory
        std::time_t rawtime;
        std::tm* timeinfo;
        char yyyymmdd [80];
        std::time(&rawtime);
        timeinfo = std::localtime(&rawtime);
        std::strftime(yyyymmdd,80,"%Y%m%d",timeinfo);
        std::string subfolder = "E:\\data\\";
        std::string yyyymmdd_string = yyyymmdd;
        std::string tempPath = subfolder + yyyymmdd_string;
        globalPath = tempPath;
        doesPathExist_ifNotCreate();
        ScanAndCount_var->setGlobalPath(globalPath);

        ui->WTRpixelTime->setValue(lround(initValueWaveTableRate*servoCycleTimeInMillisec));
        ui->ShowPath->setText(QString::fromStdString(tempPath));
        ui->pxSizeX->setMaximum((int)ScanAndCount_var->getIStepXmax());
        ui->pxSizeY->setMaximum((int)ScanAndCount_var->getIStepYmax());
    }


    ui->horizontalLine->addGraph();
    ui->horizontalLine->graph(0)->setPen(QPen(Qt::blue));

    ui->verticalLine->addGraph(ui->verticalLine->yAxis, ui->verticalLine->xAxis);//here we set that xAxis should be f(t) if we call yAxis variable t; by default, it is the other way around i.e. the standard case
    ui->verticalLine->graph(0)->setPen(QPen(Qt::green));

    //setup the color scale for the Zscan plot
    ui->image3DPlot->plotLayout()->addElement(0, 1, colorScale3Dplot); //add color scale to plot
    colorScale3Dplot->setType(QCPAxis::atRight);
    ui->image3DPlot->colorMap->setColorScale(colorScale3Dplot); // associate the color map with the color scale

    //setup the color scale for the 2D plot with ROIs
    ui->QCPimageWROI->plotLayout()->addElement(0, 1, colorScale2Dplot); //add color scale to plot
    colorScale2Dplot->setType(QCPAxis::atRight);
    ui->QCPimageWROI->colorMap->setColorScale(colorScale2Dplot); // associate the color map with the color scale

    //set the gradient for the plots
    //WARNING: HAS TO BE DONE AFTER setColorScale !!!!!
    ui->QCPimageWROI->colorMap->setGradient(rainBowPlusWhiteGradient);
    ui->QCPimageWROI->colorMap->setInterpolate(false);
    ui->image3DPlot->colorMap->setGradient(rainBowPlusWhiteGradient);
    ui->image3DPlot->colorMap->setInterpolate(false);



    // -------------------------------
    // ---- Sequence editor stuff ----
    // -------------------------------
    seqEditors_in_GUI.push_back(ui->chan0);
    seqEditors_in_GUI.push_back(ui->chan1);
    seqEditors_in_GUI.push_back(ui->chan2);
    seqEditors_in_GUI.push_back(ui->chan3);
    seqEditors_in_GUI.push_back(ui->chan4);
    seqEditors_in_GUI.push_back(ui->chan5);
    seqEditors_in_GUI.push_back(ui->chan6);
    seqEditors_in_GUI.push_back(ui->chan7);

    constStates_in_GUI.push_back(ui->constState_chan0);
    constStates_in_GUI.push_back(ui->constState_chan1);
    constStates_in_GUI.push_back(ui->constState_chan2);
    constStates_in_GUI.push_back(ui->constState_chan3);
    constStates_in_GUI.push_back(ui->constState_chan4);
    constStates_in_GUI.push_back(ui->constState_chan5);
    constStates_in_GUI.push_back(ui->constState_chan6);
    constStates_in_GUI.push_back(ui->constState_chan7);

    //connect all sequence editor to the others in order to synchronize their range
    for (int k = 0; k<8; ++k)
    {
        for (int l = 0; l <8; ++l)
        {
            if (l != k )
                connect(seqEditors_in_GUI[k]->axisRect()->axis(QCPAxis::AxisType::atBottom), SIGNAL(rangeChanged(const QCPRange&)), seqEditors_in_GUI[l], SLOT(setNewRange(const QCPRange&)));
        }
    }

    // -------------------------------
    // -------- LabJack stuff --------
    // -------------------------------


    //setup the plotting details
    ui->fastPlot->addGraph();
    ui->fastPlot->setInteraction(QCP::iRangeDrag, true);
    ui->fastPlot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 4));

    ui->slowPlot->addGraph();
    ui->slowPlot->setInteraction(QCP::iRangeDrag, true);
    ui->slowPlot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 4));

    //we know already the amount of data we will know at max ; we reserve it
    time_vals_fast.reserve(data_length_LJ);
    data_to_plot_fast.reserve(data_length_LJ);
    time_vals_slow.reserve(data_length_LJ);
    data_to_plot_slow.reserve(data_length_LJ);

    ui->extDACvalue0->setMinimum(VCA_RFSA2113_limits.low_lim);
    ui->extDACvalue0->setMaximum(VCA_RFSA2113_limits.high_lim);
    ui->extDACvalue1->setMinimum(maxLimits.low_lim);
    ui->extDACvalue1->setMaximum(maxLimits.high_lim);

    if(LJ_T7_PHYS_CONNECT)
    {
        //connect the timers for plotting the data acquired by the LabJack T7 module
        connect(acquisitionTimerFast, SIGNAL(timeout()), this, SLOT(acquireDataAndPlotFast()));
        connect(acquisitionTimerSlow, SIGNAL(timeout()), this, SLOT(acquireDataAndPlotSlow()));
    }

}

MainWindow::~MainWindow()
{
    delete ui;
    if(LJ_T7&&LJ_T7_PHYS_CONNECT) {delete LJ_T7;}
    delete acquisitionTimerFast;
    delete acquisitionTimerSlow;
}

//###############################################################################################################################################
//##                                                                                                                                           ##
//##                                                Scan Visualization Code                                                                    ##
//##                                                                                                                                           ##
//###############################################################################################################################################

//slot function through which the ScanAndCount instance commands the MainWindow to plot ( connected to signal readyTo2DPlot() )

void MainWindow::plotAfterDataAcq()
{
    //rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient
    //colorMap data has already been defined
    ui->QCPimageWROI->colorMap->rescaleDataRange();
    //rescale the key (x) and value (y) axes so the whole color map is visible
    ui->QCPimageWROI->rescaleAxes();

    ui->QCPimageWROI->replot();


    //fill vectors containing parameter/abscissa for plotting along crosshair lines
    //these vectors stay constant for a given aquired image
    fillKeyVec();
    fillValueVec();
    //set the ranges for the parameter/abscissa axis (x resp. y for plot along horizontal resp. vertical);
    ui->horizontalLine->xAxis->setRange(ui->QCPimageWROI->colorMap->data()->keyRange());
    ui->verticalLine->yAxis->setRange(ui->QCPimageWROI->colorMap->data()->valueRange());


}


//--------------------------
// functions for crosshair
//--------------------------

void MainWindow::enableCrosshair()
{
    ui->QCPimageWROI->enableCrosshair();
    connect(ui->QCPimageWROI->lineH, SIGNAL(lineMoved()), this, SLOT(replotAlongHLine()));
    connect(ui->QCPimageWROI->lineV, SIGNAL(lineMoved()), this, SLOT(replotAlongVLine()));
}


void MainWindow::disableCrosshair()
{
    ui->QCPimageWROI->disableCrosshair();
}


void MainWindow::fillKeyVec()
{
    int sizeKeys = ui->QCPimageWROI->colorMap->data()->keySize();
    keyVec.clear();
    keyVec.resize(sizeKeys); //QVector resize() function sets added values to 0 (and no way to set it to another value)
    double key;
    for (int i = 0; i<sizeKeys;++i)
    {
        ui->QCPimageWROI->colorMap->data()->cellToCoord(i, 1, &key, 0);
        keyVec[i] = key;
    }
}


void MainWindow::fillValueVec()
{
    int sizeValues = ui->QCPimageWROI->colorMap->data()->valueSize();
    valueVec.clear();//not really needed but to be sure
    valueVec.resize(sizeValues); //QVector resize() function sets added values to 0 (and no way to set it to another value)
    double value;
    for (int j = 0; j<sizeValues;++j)
    {
        ui->QCPimageWROI->colorMap->data()->cellToCoord(1, j, 0, &value);
        valueVec[j] = value;
    }
}


QVector<double> MainWindow::getVData()
{
    QVector<double> yVec;
    // need to get the cell index (from 0 to keySize-1 ) corresponding to the coordinate position of the vertical line
    // first get the graph coordinates (i.e. a double) from point1 of the vertical line, then get the cell number of that point
    //double key, value;
    int cellPos_VLine;
    QPointF Pt1 {ui->QCPimageWROI->lineV->point1->coords()};
    //ui->QCPimageWROI->colorMap->pixelsToCoords(ui->QCPimageWROI->lineV->point1->coords(), key, value);
    ui->QCPimageWROI->colorMap->data()->coordToCell(Pt1.x(), Pt1.y(), &cellPos_VLine, 0);
    //fill the vector with all the cell values along the vertical line
    for (int i = 0; i < ui->QCPimageWROI->colorMap->data()->valueSize(); ++i)
    {
        yVec.push_back(ui->QCPimageWROI->colorMap->data()->cell(cellPos_VLine, i));
    }
    return yVec;
}


QVector<double> MainWindow::getHData()
{
    QVector<double> xVec;
    // need to get the index (from 0 to valueSize -1 ) corresponding to the pixel position of the horizontal line
    // first get the graph coordinates (i.e. a double) from point1 of the horizontal line, then get the cell number of that point
    //double key, value;
    int cellPos_HLine;
    QPointF Pt1{ui->QCPimageWROI->lineH->point1->coords()};
    //ui->QCPimageWROI->colorMap->pixelsToCoords(ui->QCPimageWROI->lineH->point1->coords(), key, value);
    ui->QCPimageWROI->colorMap->data()->coordToCell(Pt1.x(), Pt1.y(), 0, &cellPos_HLine);
    //fill the vector with all the cell values along the horizontal line
    for (int i = 0; i < ui->QCPimageWROI->colorMap->data()->keySize(); ++i)
    {
        xVec.push_back(ui->QCPimageWROI->colorMap->data()->cell(i, cellPos_HLine));
    }
    return xVec;
}


void MainWindow::replotAlongHLine()
{
    QVector<double> HData = getHData();
    double Hmax = *std::max_element(HData.constBegin(), HData.constEnd());
    double Hmin = *std::min_element(HData.constBegin(), HData.constEnd());
    ui->horizontalLine->yAxis->setRange(Hmin, Hmax);
    ui->horizontalLine->graph(0)->setData(keyVec, HData);
    ui->horizontalLine->replot();
}


void MainWindow::replotAlongVLine()
{
    QVector<double> VData = getVData();
    double Vmax = *std::max_element(VData.constBegin(), VData.constEnd());
    double Vmin = *std::min_element(VData.constBegin(), VData.constEnd());
    ui->verticalLine->xAxis->setRange(Vmin, Vmax);
    ui->verticalLine->graph(0)->setData(valueVec, VData);
    ui->verticalLine->replot();
}


bool MainWindow::closedLoopCheck()
{
    if(ui->loopX->isChecked() && ui->loopY->isChecked() && ui->loopZ->isChecked())
    {
        return true;
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("One (or more) control loop(s) are open!");
        msgBox.exec();
        return false;
    }
}





// fills a line of the table showing what has been defined in the Wave Table
void MainWindow::wavGenTableLineFill(int lineNbInQTable, int pxNumber, int pxSize, QString st)
{

    //st = AxisToQString(A);
    QTableWidgetItem* cell1 = new QTableWidgetItem(st);
    ui->defdWaveTables->setItem(lineNbInQTable, 0, cell1);

    st = QString::number(pxSize);
    QTableWidgetItem* cell2 = new QTableWidgetItem(st);
    ui->defdWaveTables->setItem(lineNbInQTable, 1, cell2);

    st = QString::number(pxNumber);
    QTableWidgetItem* cell3 = new QTableWidgetItem(st);
    ui->defdWaveTables->setItem(lineNbInQTable, 2, cell3);

}

void MainWindow::moveToPosition(double xPos, double yPos)
{
    int newPositionX =  xPos*1000.;
    int newPositionY =  yPos*1000.;

    //actually move the stage to the center of the ROI
    ScanAndCount_var->moveAxisTo_VelLimit(Axis::X, newPositionX);
    ScanAndCount_var->moveAxisTo_VelLimit(Axis::Y, newPositionY);
}

//Check if selected folder exist, if not create it
void MainWindow::doesPathExist_ifNotCreate()
{
    if (!std::filesystem::exists(globalPath))
    {
        std::cout << "Path " << globalPath << " does not exist!" << std::endl;
        if(std::filesystem::create_directory(globalPath)) {
                std::cout << "Path created" << std::endl;
        }else{
            std::cout << "unable to create Path" << std::endl;
        }
    }
}


bool MainWindow::writeSeqToFile(int nbActChan, int length, double tStep_in_ns, const std::vector< std::vector<edgeProp> >& risingE_Vectors, const std::vector< std::vector<edgeProp> >& fallingE_Vectors, const std::string& path, const std::string& name)
{
    if (risingE_Vectors.size() != nbActChan || fallingE_Vectors.size() != nbActChan)
    {
        std::cout << "Error: Wrong number of vectors of rising or falling edges; this should match the number of actiated channels!" << std::endl;
        return false;
    }

    //set current working directory
    std::filesystem::path p(path);
    std::filesystem::current_path(p);

    //create output stream isntance to file
    std::ofstream fileWritten;

    std::string nowStr = getNow(); //current data and time

    //fileWritten.open(nowStr + "_Sequence_" + name + ".dat", std::ios::out | std::ios::trunc);
    std::string filename = nowStr + " Sequence " + name + ".dat";
    std::replace(filename.begin(),filename.end(), ':', ';'); //IMPORTANT: Windows file system does not accept colon ':', which we replace by ';'
    fileWritten.open(filename, std::ios::out | std::ios::trunc);

    if(fileWritten.is_open())
    {
        fileWritten << "Number of activated channels:\n" << nbActChan << '\n';
        fileWritten << "Time step (ns):\n" << tStep_in_ns << '\n' << "Clock frequency (MHz): " << 1000./tStep_in_ns << '\n';

        fileWritten << "Sequence length:\n" << length << '\n' << '\n' << '\n';

        fileWritten << "RISING EDGES:\n";
        //write how many rising edges each channel has in this sequence
        fileWritten << "Number of rising edges per channel:\n";
        for (int c=0; c<nbActChan; ++c)
        {
            fileWritten << risingE_Vectors[c].size() << '\t';
        }
        fileWritten << '\n' << '\n';
        //write the time points where the edges happen
        fileWritten << "Timestamps:\n";
        for(int c = 0; c<nbActChan; ++c)
        {
            fileWritten << "Channel #" << c << '\n';
            for(int e = 0; e<risingE_Vectors[c].size(); ++e)
            {
                fileWritten << risingE_Vectors[c][e].timestamp << '\t';
            }
            fileWritten << '\n' << '\n';
        }

        fileWritten << '\n';

        fileWritten << "FALLING EDGES:\n";
        //write how many rising edges each channel has in this sequence
        fileWritten << "Number of falling edges per channel:\n";
        for (int c=0; c<nbActChan; ++c)
        {
            fileWritten << fallingE_Vectors[c].size() << '\t';
        }
        fileWritten << '\n' << '\n';
        //write the time points where the edges happen
        fileWritten << "Timestamps:\n";
        for(int c = 0; c<nbActChan; ++c)
        {
            fileWritten << "Channel #" << c << '\n';
            for(int e = 0; e<fallingE_Vectors[c].size(); ++e)
            {
                fileWritten << fallingE_Vectors[c][e].timestamp << '\t';
            }
            fileWritten << '\n' << '\n';
        }

    }
    else
    {
        std::cout << "File failed to open!" << std::endl;
        return false;
    }
    fileWritten.close();

    return true;
}

bool MainWindow::loadSeqFromFile(std::filesystem::path filePath, std::string filename, std::vector< std::vector<edgeProp> >& risingE_outVec, std::vector< std::vector<edgeProp> >& fallingE_outVec)
{
    std::filesystem::current_path(filePath);

    std::ifstream inputF;
    inputF.open(filename);

    std::string line;

    std::getline(inputF, line);
    std::getline(inputF, line); //--> this should be the number of activated channels
    int P = atoi(line.c_str());
    risingE_outVec.resize(P);
    fallingE_outVec.resize(P);

    std::getline(inputF, line);
    std::getline(inputF, line); //--> this should be the time step in ns
    double dt = atof(line.c_str());

    std::getline(inputF, line);

    std::getline(inputF, line);
    std::getline(inputF, line); //--> this should be the length in samples i.e. number of time points
    int L = atoi(line.c_str());

    std::string st1 = "Number of rising edges per channel";
    for (; line.substr(0, st1.size()) != st1 && !inputF.eof(); std::getline(inputF, line))
    {
    }
    //now get the next line with the numbers of edges and parse it into ints
    std::getline(inputF, line); //--> list of the number of edges on each channel

    //now take the new line as a tab-separated stream and parse it as such
    std::istringstream iss_nb;
    iss_nb.str(line);
    std::string edgeNb;
    edgeProp e {0, nullptr};
    int k = 0;

    std::getline(iss_nb, edgeNb, '\t');//there is at least one channel obviously
    do
    {
        //std::cout << atoi(edgeNb.c_str()) << ", k=" << k << std::endl;
        std::vector<edgeProp> v (atoi(edgeNb.c_str()), e);
        if(k<risingE_outVec.size())
        {
            risingE_outVec[k] = v;
        }
        ++k;
        std::getline(iss_nb, edgeNb, '\t');
    }while(!iss_nb.eof());

    std::cout << "rise outVec->" << risingE_outVec.size() << std::endl;
    for(int i = 0; i<risingE_outVec.size(); ++i)
    {
        std::cout << risingE_outVec[i].size() << std::endl;
    }

    //now we are done initializing the vectors of edges

    //skip 2 lines
    std::getline(inputF, line);
    std::getline(inputF, line);

    std::string timestamp;
    std::istringstream iss_t;

    //successively parse the lines containing the timestamps for the rising edges on each channel
    std::cout << "Rising edges first:" << std::endl;
    for(int chInd = 0; chInd<P; ++chInd)
    {
        std::getline(inputF, line); // --> line "Channel #"
        std::getline(inputF, line); // --> line with edges on chInd

        iss_t.clear();
        iss_t.str(line);
        k = 0;
        if(risingE_outVec[chInd].size()>0)//there is at least 1 edge on this channel
        {
            std::getline(iss_t, timestamp, '\t');
            do
            {
                std::cout << atof(timestamp.c_str()) << ", k=" << k << std::endl;
                if(k<risingE_outVec[chInd].size())
                    risingE_outVec[chInd][k].timestamp = atof(timestamp.c_str());
                ++k;
                std::getline(iss_t, timestamp, '\t');
            }while(!iss_t.eof());
        }
        std::getline(inputF, line);// --> empty line
    }

    //---------------------------------------------------------------------------------------
    //THEN REPEAT THE SAME CYCLES FOR THE FALLING EDGES

    std::string st2 = "Number of falling edges per channel";
    for (; line.substr(0, st2.size()) != st2 && !inputF.eof(); std::getline(inputF, line))
    {
    }

    std::cout << "Falling edges from now:" << std::endl;
    //now get the next line with the numbers of edges and parse it into ints
    std::getline(inputF, line); //--> list of the number of edges on each channel

    //now take the new line as a tab-separated stream and parse it as such
    iss_nb.clear();
    iss_nb.str(line);
    k = 0;

    std::getline(iss_nb, edgeNb, '\t');//there is at least one channel obviously
    do
    {
        //std::cout << atoi(edgeNb.c_str()) << ", k=" << k << std::endl;
        std::vector<edgeProp> v (atoi(edgeNb.c_str()), e);
        if(k<fallingE_outVec.size())
        {
            fallingE_outVec[k] = v;
        }
        ++k;
        std::getline(iss_nb, edgeNb, '\t');
    }while(!iss_nb.eof());

    //skip 2 lines
    std::getline(inputF, line);
    std::getline(inputF, line);

    //successively parse the lines containing the timestamps for the falling edges on each channel
    for(int chInd = 0; chInd<P; ++chInd)
    {
        std::getline(inputF, line); // --> line "Channel #"
        std::cout << "is it Chan#? " << line << std::endl;
        std::getline(inputF, line); // --> line with edges on chInd

        iss_t.clear();
        iss_t.str(line);
        k = 0;
        if(fallingE_outVec[chInd].size()>0)//there is at least 1 edge on this channel
        {
            std::getline(iss_t, timestamp, '\t');
            do
            {
                std::cout << atof(timestamp.c_str()) << ", k=" << k << std::endl;
                if(k<fallingE_outVec[chInd].size())
                    fallingE_outVec[chInd][k].timestamp = atof(timestamp.c_str());
                ++k;
                std::getline(iss_t, timestamp, '\t');
            }while(!iss_t.eof());
        }
        std::getline(inputF, line);// --> empty line
    }

    //set the right parameters for plotting the sequence data and reset all plot data to 0
    ui->seqLength->setValue(L);
    ui->seqTimeStep->setValue(dt);
    for (int i=0; i<8; ++i)
    {
        seqEditors_in_GUI[i]->resetData(dt, (L-1)*dt);
    }


    //now set the plot data with the right edges in each channel using
    //the same routine as the parsing and plotting from QCPSequenceData
    //and replot each channel
    for (int k = 0; k<P; ++k)
    {
        if(risingE_outVec[k].size()!=fallingE_outVec[k].size())
        {
            std::cout << "Error: number of rising and falling edges not equal!\n" << "Add an adequate number of raising and falling edges!\n\n" << std::endl;
            return false;
        }

        QCPGraphData* found_plot_rise = seqEditors_in_GUI[k]->seqData->data()->begin();
        QCPGraphData* found_plot_fall = seqEditors_in_GUI[k]->seqData->data()->begin()-1;
        for (int i=0; i<risingE_outVec[k].size(); ++i)
        {
                //get the i-th rising edge
                double risingEdge = risingE_outVec[k][i].timestamp;
                //get the corresponding fallingEdge
                double fallingEdge = fallingE_outVec[k][i].timestamp;
                if (fallingEdge <= risingEdge)//check that the falling edge is correct
                {
                    std::cout << "Error: corresponding falling edge comes before rising edge!\n" << std::endl;
                    return false;
                }
                //find the index of the rising edge in the plot data(first falling edge "is index -1")
                double Tedge = risingEdge;
                auto testEdge = [&Tedge](QCPGraphData data){return std::abs(data.key-Tedge)<0.0001;};
                found_plot_rise = std::find_if(seqEditors_in_GUI[k]->seqData->data()->begin(), seqEditors_in_GUI[k]->seqData->data()->end(), testEdge);
                //find the index of the falling edge in the plot data
                Tedge = fallingEdge;
                found_plot_fall = std::find_if(seqEditors_in_GUI[k]->seqData->data()->begin(), seqEditors_in_GUI[k]->seqData->data()->end(), testEdge);
                std::for_each(found_plot_rise+1, found_plot_fall+1, [](QCPGraphData& data){(&data)->value = 1;});
                //add a point at the rising edge and at the falling edge
                QVector<double> new_keys = {risingEdge, fallingEdge};
                QVector<double> new_values = {1., 0.};
                seqEditors_in_GUI[k]->seqData->addData(new_keys, new_values, true);
                std::cout << "After add_data:\n" << "found_plot_rise: " << found_plot_rise << '\n' << "found_plot_fall: " << found_plot_fall << std::endl;
                //replot
                seqEditors_in_GUI[k]->replot();

        }
    }

    return true;
}



void MainWindow::pulseOutput()
{
    int nb = ui->pulseNb->value();
    int pulseLength = ui->pulseLength->value();
    int cycleLength = ui->totalCycleVal->value();

    LJ_T7->pulseSetupAndEnable(nb, pulseLength, cycleLength);

    //std::cout << "Outputting pulses" << std::endl;

    //make a pause of at least 200ms
    int totalTime = nb*cycleLength;
    int pause;
    if (totalTime < 200000)
        pause = 200;
    else
        pause = (int)((totalTime*1.2)/1000);
    std::chrono::duration<int, std::milli>Tsleep (pause);
    std::this_thread::sleep_for(Tsleep);

    LJ_T7->pulseDisable();

    //std::cout << "Exiting pulse out function" << std::endl;
}

void MainWindow::acquireDataAndPlotFast()
{
    //actually read out and increment data vector
    double val = LJ_T7->analogReadOut(anChanFast1);

    //std::cout << "size of data_to_plot_fast:" << data_to_plot_fast.size() << std::endl;

    // if the data has reached its max size, just shift it and dump the oldest value
    // otherwise add one value to the vector
    if (data_to_plot_fast.size() < data_length_LJ)
    {
        data_to_plot_fast.push_back(val);
    }
    else
    {
        //to avoid implementing a new function and make things short, std::rotate is used
        std::rotate(data_to_plot_fast.begin(), data_to_plot_fast.begin() + 1, data_to_plot_fast.end());
        data_to_plot_fast[data_to_plot_fast.size()-1] = val;
    }

    //update time values (x axis)
    size_t L = data_to_plot_fast.size();
    if(time_vals_fast.size() < data_length_LJ)
        time_vals_fast.insert(time_vals_fast.begin(),- time_step_acq_fast * (double)(L-1)/1000.);

    //min_element and max_element return an interator to the min or max element of the vector
    double data_min = *std::min_element(data_to_plot_fast.begin(), data_to_plot_fast.end());
    double data_max = *std::max_element(data_to_plot_fast.begin(), data_to_plot_fast.end());

    ui->fastPlot->graph(0)->setData(time_vals_fast, data_to_plot_fast);
    ui->fastPlot->xAxis->setLabel("x");
    ui->fastPlot->yAxis->setLabel("y");
    // set axes ranges, so we see all the data:
    ui->fastPlot->xAxis->setRange(time_vals_fast[0], time_vals_fast[time_vals_fast.size()-1]);
    double data_middle = (data_max + data_min)/2;
    double data_range = data_max - data_min;
    ui->fastPlot->yAxis->setRange(data_middle - 1.2 * data_range, data_middle + 1.2 * data_range);

    ui->fastPlot->replot();
}

void MainWindow::acquireDataAndPlotSlow()
{
    //actually read out and increment data vector
    double val = LJ_T7->analogReadOut(anChanSlow1);
    //std::cout << "slow meas value : " << val << std::endl;
    // if the data has reached its max size, just shift it and dump the oldest value
    // otherwise add one value to the vector
    if (data_to_plot_slow.size() < data_length_LJ)
    {
        data_to_plot_slow.push_back(val);
    }
    else
    {
        //to avoid implementing a new function and make things short, std::rotate is used
        std::rotate(data_to_plot_slow.begin(), data_to_plot_slow.begin() + 1, data_to_plot_slow.end());
        data_to_plot_slow[data_to_plot_slow.size()-1] = val;
    }

    //update time values (x axis)
    size_t L = data_to_plot_slow.size();

    if(time_vals_slow.size() < data_length_LJ)
        time_vals_slow.insert(time_vals_slow.begin(),- time_step_acq_slow * (double)(L-1)/60);
    /*
    std::cout << "slow time step = " << time_step_acq_slow << '\n';
    std::cout << "size of time_vals_slow:" << time_vals_slow.size() << '\n';
    std::cout << "size of data_to_plot_slow:" << data_to_plot_slow.size() << '\n' << std::endl;
    */

    //min_element and max_element return an interator to the min or max element of the vector
    double data_min = *std::min_element(data_to_plot_slow.begin(), data_to_plot_slow.end());
    double data_max = *std::max_element(data_to_plot_slow.begin(), data_to_plot_slow.end());

    ui->slowPlot->graph(0)->setData(time_vals_slow, data_to_plot_slow);
    ui->slowPlot->xAxis->setLabel("x");
    ui->slowPlot->yAxis->setLabel("y");
    // set axes ranges, so we see all the data:
    if(time_vals_slow.size()>1)
    ui->slowPlot->xAxis->setRange(time_vals_slow[0], time_vals_slow[time_vals_slow.size()-1] );
    double data_middle = (data_max + data_min)/2;
    double data_range = data_max - data_min;
    ui->slowPlot->yAxis->setRange(data_middle - 1.2 * data_range, data_middle + 1.2 * data_range);

    ui->slowPlot->replot();

}




void MainWindow::prepareRabiExp(std::vector<double> excTs, double readOutTime, bool ref, double piTime)
{
    //for ()
    //triggerGen.RabiExp();
}



//============================================================================
//                          Slots for GUI below
//============================================================================


void MainWindow::on_crossHairBox_stateChanged(int newState)
{
    if(newState)
    {
        enableCrosshair();
    }
    else if (ui->QCPimageWROI->lineH != nullptr && ui->QCPimageWROI->lineV != nullptr)
    {
        disableCrosshair();
    }
}

//-----------------------------------------
// slots for Regions of Interest (ROI)
//-----------------------------------------


void MainWindow::on_setROILegend_clicked(bool checked)
{
    ui->QCPimageWROI->enableROILegend(checked);
    ui->QCPimageWROI->replot();
}


void MainWindow::on_addROI_clicked()
{
    int n = ui->QCPimageWROI->ROInumber();

    double xstep = ui->QCPimageWROI->singleCellStepX();
    double ystep = ui->QCPimageWROI->singleCellStepY();
    int lx = ui->QCPimageWROI->colorMap->data()->keySize();
    int ly = ui->QCPimageWROI->colorMap->data()->valueSize();
    double Xcenter = ui->QCPimageWROI->colorMap->data()->keyRange().lower + (lx/5)*xstep + (n%4)*(lx/5)*xstep;
    double Ycenter = ui->QCPimageWROI->colorMap->data()->valueRange().lower + (ly/5)*ystep + (n/4)*(lx/5)*ystep;

    QPointF center{Xcenter, Ycenter};
    // use pixel to set ROI's
    //ui->QCPimageWROI->addROI(center, 7, 7);
    // use micrometer to set ROI's
    ui->QCPimageWROI->addROI(center, 0.3, 0.3);

    ui->QCPimageWROI->replot();
}


void MainWindow::on_removeROI_clicked()
{
    ui->QCPimageWROI->removeROI();
    ui->QCPimageWROI->replot();
}


void MainWindow::on_resetAxes_clicked()
{
    ui->QCPimageWROI->xAxis->setRange(ui->QCPimageWROI->colorMap->data()->keyRange());
    ui->QCPimageWROI->yAxis->setRange(ui->QCPimageWROI->colorMap->data()->valueRange());

    //keep position of "you are here !" sign with respect to ROI
    if (ui->QCPimageWROI->YouAreHere->visible())
    {
        QPointF here = ui->QCPimageWROI->stagePosition->position->pixelPosition() - QCPRectROI::label_offset;
        ui->QCPimageWROI->YouAreHere->position->setCoords(here);
    }
    //keep position of size label with respect to the ROI
    for (auto ROI : ui->QCPimageWROI->ROIStore)
    {
        QPointF sizepos = ROI->bottom->pixelPosition() - QCPRectROI::label_offset;
        ROI->sizeLabel->position->setCoords(sizepos);
    }

    ui->QCPimageWROI->replot();
}


void MainWindow::on_LoadDATFile_clicked()
{
    //LoadFileCreatMosaic_var->loadDataFromZScan("E:/data/20200515/Z-Scan1");
    QString fileToOpen = QFileDialog::getOpenFileName(this, "Open file" , QString::fromStdString(globalPath.string()));
    if(!fileToOpen.isEmpty()&& !fileToOpen.isNull()){
        LoadFileCreatMosaic_var->loadDataFromSingleScan(fileToOpen.toStdString());
        plotAfterDataAcq();
        ScanAndCount_var->currentImageDate_yyyymmddHHMMSS = LoadFileCreatMosaic_var->getFileNameOfLoaded2D();
        ScanAndCount_var->currentImagePath = LoadFileCreatMosaic_var->getPathOfLoaded2D();
    }
}



void MainWindow::on_setMaxPlot_clicked()
{
    double maxValPLot = ui->maxValPlot->value();
    QCPRange r(0, maxValPLot);
    ui->QCPimageWROI->colorMap->setDataRange(r);
    ui->QCPimageWROI->replot();
}

void MainWindow::on_setMaxPlotAuto_clicked()
{
    ui->QCPimageWROI->colorMap->rescaleDataRange();
    ui->QCPimageWROI->replot();
}



void MainWindow::on_PDFsave_clicked()
{
    QString Path = QString::fromStdString(ScanAndCount_var->currentImagePath);
    QString Name = QString::fromStdString(ScanAndCount_var->currentImageDate_yyyymmddHHMMSS);
    ui->QCPimageWROI->savePdf(Path+"\\"+Name+".pdf");
}

void MainWindow::on_PNGsave_clicked()
{
    QString Path = QString::fromStdString(ScanAndCount_var->currentImagePath);
    QString Name = QString::fromStdString(ScanAndCount_var->currentImageDate_yyyymmddHHMMSS);
    ui->QCPimageWROI->savePng(Path+"\\"+Name+".png");
}

void MainWindow::on_getCountsPerSec_clicked()
{
    ui->showCountsPerSec->display(ScanAndCount_var->getCurrentCountsFromTT()/1000);
}



void MainWindow::on_readValues_clicked()
{
    if(closedLoopCheck())
    {
        std::vector<double> positions {ScanAndCount_var->readAllPositions_Closed()};
        ui->readXVal->display(positions[0]);
        ui->setValX->setValue(positions[0]);
        ui->readYVal->display(positions[1]);
        ui->setValY->setValue(positions[1]);
        ui->readZVal->display(positions[2]);
        ui->setValZ->setValue(positions[2]);
    }
}

void MainWindow::on_STOPButton_clicked()
{

    ScanAndCount_var->on_STOPButton_clicked_scanandcount();

    //display positions after stopping
    on_readValues_clicked();
}

void MainWindow::on_loopX_stateChanged(int state)
{

    ScanAndCount_var->on_loopAXIS_stateChanged(state, Axis::X);

    //on_readValues_clicked();
}

void MainWindow::on_loopY_stateChanged(int state)
{

    ScanAndCount_var->on_loopAXIS_stateChanged(state, Axis::Y);

    //on_readValues_clicked();
}

void MainWindow::on_loopZ_stateChanged(int state)
{

    ScanAndCount_var->on_loopAXIS_stateChanged(state, Axis::Z);

    //on_readValues_clicked();
}

void MainWindow::on_setValX_editingFinished()
{
    if(closedLoopCheck())
    {
        int newPos = ui->setValX->value();
        ScanAndCount_var->moveAxisTo_VelLimit(Axis::X, newPos);
    }
}

void MainWindow::on_stepX_valueChanged(int newStep)
{
    ui->setValX->setSingleStep(newStep);
}

void MainWindow::on_setValY_editingFinished()
{
    if(closedLoopCheck())
    {
        int newPos = ui->setValY->value();
        ScanAndCount_var->moveAxisTo_VelLimit(Axis::Y, newPos);
    }
}

void MainWindow::on_stepY_valueChanged(int newStep)
{
    ui->setValY->setSingleStep(newStep);
}

void MainWindow::on_setValZ_editingFinished()
{
    if(closedLoopCheck())
    {
        int newPos = ui->setValZ->value();
        ScanAndCount_var->moveAxisTo_VelLimit(Axis::Z, newPos);
    }
}

void MainWindow::on_stepZ_valueChanged(int newStep)
{
    ui->setValZ->setSingleStep(newStep);
}


void MainWindow::on_TwoDScanSetup_clicked()
{

    int Lx = ui->pxNbX->value();
    int Ly = ui->pxNbY->value();
    int Xstep = ui->pxSizeX->value();
    int Ystep = ui->pxSizeY->value();

    ScanAndCount_var->on_TwoDScanSetup_clicked_scanandcount(Lx, Ly, Xstep, Ystep);

    QString X_str = "X";
    QString Y_str = "Y";
    wavGenTableLineFill(0, Lx, Xstep, X_str);
    wavGenTableLineFill(1, Ly, Ystep, Y_str);

}


void MainWindow::on_TwoDScanStart_clicked()
{
    if(closedLoopCheck())
    {
        //ScanAndCount_var->on_TwoDScanStart_clicked_scanandcount();

        std::thread th(&ScanAndCount::on_TwoDScanStart_clicked_scanandcount, ScanAndCount_var);
        th.detach();
    }
}


void MainWindow::on_ThreeDScanStart_clicked()
{

    int Lz = ui->pxNbZ->value();
    int Zstep = ui->pxSizeZ->value();
    if(closedLoopCheck())
    {
        ScanAndCount_var->set3DScanParameters(Lz, Zstep);
        std::thread th(&ScanAndCount::on_ThreeDScanStart_clicked_scanandcount, ScanAndCount_var);
        th.detach();
    }
}

void MainWindow::on_WaveTableRateButton_clicked()
{
    int pxTimeInMillisec = ui->WTRpixelTime->value();
    if(pxTimeInMillisec<minTimePerPtInMillisec)
    {
        std::cout << "Time too short!\nPiezo table has a significant chance of not stabilising to positions." << std::endl;
        ui->WTRpixelTime->setValue(lround(minTimePerPtInMillisec));
    }
    else
    {
        double WTR_inServoCycles = ((double)pxTimeInMillisec)/servoCycleTimeInMillisec;
        std::cout << "Setting Wave Table Rate to "  << lround(WTR_inServoCycles) << " cycles." << std::endl;
        ScanAndCount_var->setWaveTableRate(lround(WTR_inServoCycles));
    }
}


void MainWindow::on_LoadDatFile3DScan_clicked()
{
    QString directoryToOpen = QFileDialog::getExistingDirectory(this, "Open directory" , QString::fromStdString(globalPath.string()));
    if(!directoryToOpen.isEmpty()&& !directoryToOpen.isNull()){
        LoadFileCreatMosaic_var->loadDataFromZScan(directoryToOpen.toStdString());
    }
}

void MainWindow::on_setMaxPlot_3D_clicked()
{
    double maxValPLot = ui->maxValPlot_3D->value();
    QCPRange r(0, maxValPLot);
    ui->image3DPlot->colorMap->setDataRange(r);
    ui->image3DPlot->replot();
}

void MainWindow::on_setMaxPlot_3D_auto_clicked()
{
    std::cout << "Range: lower=" << ui->image3DPlot->colorMap->dataRange().lower << ", upper=" << ui->image3DPlot->colorMap->dataRange().upper;

    ui->image3DPlot->colorMap->rescaleDataRange();
    ui->image3DPlot->replot();
}

void MainWindow::on_PNGsave3D_clicked()
{
    QString Path = QString::fromStdString(LoadFileCreatMosaic_var->getPathOfLoaded3D());
    QString Date = QString::fromStdString(LoadFileCreatMosaic_var->getFileNameOfLoaded3D());
    ui->image3DPlot->savePng(Path+"\\"+Date+".png");
}

void MainWindow::on_PDFsave3D_clicked()
{
    QString Path = QString::fromStdString(LoadFileCreatMosaic_var->getPathOfLoaded3D());
    QString Date = QString::fromStdString(LoadFileCreatMosaic_var->getFileNameOfLoaded3D());
    ui->image3DPlot->savePdf(Path+"\\"+Date+".pdf");
}




void MainWindow::on_STOPbutton_clicked()
{
    M8195A_var->stopGen();
}


void MainWindow::on_RUNbutton_clicked()
{
    //std::vector<double> positions {ScanAndCount_var->readAllPositions_Closed()};
    M8195A_var->run();
}

void MainWindow::on_loadWaveformAndSeqTable_clicked()
{
    int loops = ui->loopNbWaveForm->value();

    double excite = ui->excTime->value();
    ViConstString chan = "1";

    double Fstart = ui->scanStartFreq->value();
    double Fend = ui->scanStopFreq->value();
    int stepNb = ui->scanStepNb->value();

    auto frequencies = linearFreqArray(Fstart*MHz_M8195A, Fend*MHz_M8195A, stepNb);

    std::vector<double> times (frequencies.size(), excite*microsec_M8195A);
    std::vector<double> phases (frequencies.size(), 0);
    std::vector<double> amplitudes (frequencies.size(), 1);

    ViStatus status = M8195A_var->loadSineArrayWaveformAndSeqTable(chan, frequencies, maxSampleRate, times, loops, phases, amplitudes);
    /*
    PulsedMeasurementsAndData_var->currentODMRParams.loopNb=loops;
    PulsedMeasurementsAndData_var->currentODMRParams.stepNb=stepNb+1;
    PulsedMeasurementsAndData_var->currentODMRParams.FreqEnd=Fend;
    PulsedMeasurementsAndData_var->currentODMRParams.FreqStart=Fstart;
    PulsedMeasurementsAndData_var->currentODMRParams.excitTime=excite;
    */
}

void MainWindow::on_triggerMode_currentIndexChanged(const QString &mode)
{
    M8195A_var->setTriggermode(mode);
}


// ----------------------------------------------------
// ---------  Functions for digital I/O card  ---------
// ----------------------------------------------------

void MainWindow::on_initM2i_clicked()
{
    M2i7005_var->M2iInit();

    QMessageBox IOCardConnect;
    IOCardConnect.setText("IO Card is conneted");
    IOCardConnect.exec();
}

void MainWindow::on_confM2i_clicked()
{
    int actChN = next_power_of_2(ui->chanNumber->value());
    std::cout << next_power_of_2(ui->chanNumber->value()) << " activated channels" << std::endl;

    int64 mask;

    switch(actChN)
    {
        case 1: mask = (int64)1; break;
        case 2: mask = (int64)0x3; break;
        case 4: mask = (int64)0xF; break;
        case 8: mask = (int64)0xFF; break;
        case 16: mask = (int64)0xFFFF; break;
        default: std::cout << "Error: Channel mask is not allowed !" << std::endl; return;
    }

    int loop_nb = ui->loopNb->value();
    int frequency_MHz = (int)(1000./ui->chan0->step);
    int length_in_samples = lround(ui->chan0->Tmax/ui->chan0->step) + 1;

    M2i7005_var->M2iSetup_StdReplay(mask, frequency_MHz, false, false, loop_nb, length_in_samples);
}

void MainWindow::on_loadData_clicked()
{

    //M2iLoadIntoMemoryFromGui(ui->chanNumber->value());
}

void MainWindow::on_outputM2i_clicked()
{
    M2i7005_var->M2iOutputData();
}

void MainWindow::on_closeM2i_clicked()
{
//    ui->initM2i->setEnabled(true);
//    ui->closeM2i->setEnabled(false);
    M2i7005_var->M2iClose();
}

void MainWindow::on_setPermWord_clicked()
{
    int W = 0;
    for (int c=0; c<8; ++c)
    {
        W += (1<<c)*constStates_in_GUI[c]->isChecked();
    }
    //std::cout << integ_to_binStr((uint16_t) W) << std::endl;

    M2i7005_var->M2iSetConstLvl(8, (uint16_t) W);
}


// ----------------------------------------------------
// ----------   Functions for sequence editor   -------
// ----------------------------------------------------

void MainWindow::on_writeFileTestButton_clicked()
{
    std::vector< std::vector<edgeProp> > rise;
    std::vector< std::vector<edgeProp> > fall;

    rise.push_back(seqEditors_in_GUI[0]->seqData->risingEdges);
    rise.push_back(seqEditors_in_GUI[1]->seqData->risingEdges);

    fall.push_back(seqEditors_in_GUI[0]->seqData->fallingEdges);
    fall.push_back(seqEditors_in_GUI[1]->seqData->fallingEdges);

    double dt = seqEditors_in_GUI[0]->step;
    int length = lround(seqEditors_in_GUI[0]->Tmax/dt)+1;

    writeSeqToFile(2, length, dt, rise, fall, "E:\\data\\tests_sequences\\", "testFile");
}

void MainWindow::on_loadFileTestButton_clicked()
{
    std::filesystem::path path("E:\\data\\tests_sequences\\");
    std::string name = "2020-07-21 20;03;56 Sequence testFile.dat";

    //now load edges from file into vector ris & fal
    std::vector< std::vector<edgeProp> > ris;
    std::vector< std::vector<edgeProp> > fal;
    loadSeqFromFile(path, name, ris, fal);

    //put the loaded vectors into te rising and falling edges member vectors of the QCPSequenceData
    for (int r = 0; r<8; ++r)
    {
        if(r<ris.size())
            seqEditors_in_GUI[r]->seqData->risingEdges = ris[r];
        else
            seqEditors_in_GUI[r]->seqData->risingEdges.resize(0);
    }
    for (int f =0; f<8; ++f)
    {
        if(f<fal.size())
            seqEditors_in_GUI[f]->seqData->fallingEdges = fal[f];
        else
            seqEditors_in_GUI[f]->seqData->fallingEdges.resize(0);
    }

}

void MainWindow::on_parseButton0_clicked()
{
    ui->chan0->seqData->parseAndPlot();
}

void MainWindow::on_parseButton1_clicked()
{
    ui->chan1->seqData->parseAndPlot();
}

void MainWindow::on_parseButton2_clicked()
{
    ui->chan2->seqData->parseAndPlot();
}

void MainWindow::on_parseButton3_clicked()
{
    ui->chan3->seqData->parseAndPlot();
}

void MainWindow::on_parseButton4_clicked()
{
    ui->chan4->seqData->parseAndPlot();
}

void MainWindow::on_parseButton5_clicked()
{
    ui->chan5->seqData->parseAndPlot();
}

void MainWindow::on_parseButton6_clicked()
{
    ui->chan6->seqData->parseAndPlot();
}

void MainWindow::on_parseButton7_clicked()
{
    ui->chan7->seqData->parseAndPlot();
}

void MainWindow::on_changeSeqParams_clicked()
{
    double Tstep = ui->seqTimeStep->value();
    int seqL = ui->seqLength->value();
    int seqL_rounded;
    if (64*(seqL/64) == seqL)
        seqL_rounded = seqL;
    else
        seqL_rounded = (seqL/64 + 1)*64;
    std::cout << "new length: " << seqL_rounded << std::endl;

    int max_time = (seqL_rounded-1) * Tstep;
    std::cout << "new max T: " << max_time << std::endl;


    //set the plot data to 0 everywhere
    //reset the rising and falling edge vectors to length 0
    for (int i=0; i<8; ++i)
    {
        seqEditors_in_GUI[i]->resetData(Tstep, max_time);
    }
}



void MainWindow::on_connectLabjack_clicked()
{
    if(!LJ_T7 && LJ_T7_PHYS_CONNECT)
    {
        LJ_T7 = new LabJackT7();
        connect(LJ_T7, SIGNAL(valueRead(double)), ui->lastAnValue, SLOT(display(double)));
        connect(LJ_T7, SIGNAL(dutyCycleVal(double)), ui->dutyCycle, SLOT(display(double)));
    }
}

void MainWindow::on_disconnectLabjack_clicked()
{
    if(LJ_T7)
        delete LJ_T7;
    LJ_T7 = nullptr;
}

void MainWindow::on_startMeasFast_clicked()
{
    time_step_acq_fast = ui->fastTimerVal->value();
    if(!acquisitionTimerFast->isActive())
    {
        acquisitionTimerFast->start(time_step_acq_fast);
        ui->fastMeasStatus->setStyleSheet("QLabel { background : yellow }");
        ui->fastMeasStatus->setText("Running...");
        ui->fastMeasStatus->show();
    }
    else
        std::cout << "Measurement is currently running !" << std::endl;
}

void MainWindow::on_stopMeasFast_clicked()
{
    if(acquisitionTimerFast->isActive())
    {
        acquisitionTimerFast->stop();
        ui->fastMeasStatus->setStyleSheet("QLabel { background : rgb(240,240,240)}");
        ui->fastMeasStatus->setText("Not measuring.");
        ui->fastMeasStatus->show();
    }
    else
        std::cout << "Measurement is not running !" << std::endl;
}

void MainWindow::on_startMeasSlow_clicked()
{
    time_step_acq_slow = ui->slowTimerVal->value();
    if(!acquisitionTimerSlow->isActive())
    {
        acquisitionTimerSlow->start(time_step_acq_slow*1000);
        ui->slowMeasStatus->setStyleSheet("QLabel { background : yellow }");
        ui->slowMeasStatus->setText("Running...");
        ui->slowMeasStatus->show();
    }
    else
        std::cout << "Measurement is currently running !" << std::endl;
}

void MainWindow::on_stopMeasSlow_clicked()
{
    if(acquisitionTimerSlow->isActive())
    {
        acquisitionTimerSlow->stop();
        ui->slowMeasStatus->setStyleSheet("QLabel { background : rgb(240,240,240)}");
        ui->slowMeasStatus->setText("Not measuring.");
        ui->slowMeasStatus->show();
    }
    else
        std::cout << "Measurement is not running !" << std::endl;
}

void MainWindow::on_extDACvalue0_valueChanged(double V)
{
    if(LJ_T7)
        LJ_T7->extDAC_setVoltage(0, V, VCA_RFSA2113_limits);
}

void MainWindow::on_extDACvalue1_valueChanged(double V)
{
    if(LJ_T7)
        LJ_T7->extDAC_setVoltage(1, V, maxLimits);
}

void MainWindow::on_stepSizeExtDAC0_valueChanged(double step0)
{
    ui->extDACvalue0->setSingleStep(step0);
}

void MainWindow::on_stepSizeExtDAC1_valueChanged(double step1)
{
    ui->extDACvalue1->setSingleStep(step1);
}



void MainWindow::on_pulseOut_clicked()
{
    pulseOutput();
}
