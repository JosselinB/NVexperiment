QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    labjackt7.cpp \
    libs/Spectrum_Instr/common/ostools/spcm_md5.cpp \
    libs/Spectrum_Instr/common/ostools/spcm_ostools_win.cpp \
    libs/Spectrum_Instr/common/spcm_lib_card.cpp \
    libs/Spectrum_Instr/common/spcm_lib_data.cpp \
    libs/Spectrum_Instr/sb5_file/sb5_file.cpp \
    loadfilecreatmosaic.cpp \
    m2icard.cpp \
    m8195adev.cpp \
    main.cpp \
    mainwindow.cpp \
    qcpcrosshair.cpp \
    qcpmovablestraightline.cpp \
    qcpmovabletracer.cpp \
    qcprectroi.cpp \
    qcpsequencedata.cpp \
    qcustomplot.cpp \
    qcustomplotwithroi.cpp \
    qseqeditor.cpp \
    scanandcount.cpp \
    trigsequencegen.cpp

HEADERS += \
    labjackt7.h \
    libs/LabJack/LJM_Utilities.h \
    libs/LabJack/LabJackM.h \
    libs/PI/include/PI_GCS2_DLL.h \
    libs/PI/include/PI_GCS2_DLL_PF.h \
    libs/Spectrum_Instr/c_header/dlltyp.h \
    libs/Spectrum_Instr/c_header/errors.h \
    libs/Spectrum_Instr/c_header/regs.h \
    libs/Spectrum_Instr/c_header/spcerr.h \
    libs/Spectrum_Instr/c_header/spcm_drv.h \
    libs/Spectrum_Instr/common/ostools/spcm_md5.h \
    libs/Spectrum_Instr/common/ostools/spcm_ostools.h \
    libs/Spectrum_Instr/common/ostools/spcm_oswrap.h \
    libs/Spectrum_Instr/common/spcm_lib_card.h \
    libs/Spectrum_Instr/common/spcm_lib_data.h \
    libs/Spectrum_Instr/sb5_file/sb5_file.h \
    libs/Swabian_Instruments/Time_Tagger/include/Iterators.h \
    libs/Swabian_Instruments/Time_Tagger/include/TimeTagger.h \
    loadfilecreatmosaic.h \
    m2icard.h \
    m8195adev.h \
    mainwindow.h \
    qcpcrosshair.h \
    qcpmovablestraightline.h \
    qcpmovabletracer.h \
    qcprectroi.h \
    qcpsequencedata.h \
    qcustomplot.h \
    qcustomplotwithroi.h \
    qseqeditor.h \
    scanandcount.h \
    trigsequencegen.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


Debug:QMAKE_CXXFLAGS += /MT
Release:QMAKE_CXXFLAGS += /MT



########################## E-725 PIEZO CONTROLLER ##################################

win32: LIBS += -L$$PWD/libs/PI/ -lPI_GCS2_DLL_x64

INCLUDEPATH += $$PWD/libs/PI/include
DEPENDPATH += $$PWD/libs/PI/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/libs/PI/PI_GCS2_DLL_x64.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/libs/PI/libPI_GCS2_DLL_x64.a



################### TIME TAGGER 20 #############################

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/'libs/Swabian_Instruments/Time_Tagger/x64/' -lTimeTagger
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/'libs/Swabian_Instruments/Time_Tagger/x64/' -lTimeTaggerd

INCLUDEPATH += $$PWD/'libs/Swabian Instruments/Time_Tagger/include'
DEPENDPATH += $$PWD/'libs/Swabian Instruments/Time_Tagger/include'

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/'libs/Swabian_Instruments/Time_Tagger/x64/libTimeTagger.a'
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/'libs/Swabian_Instruments/Time_Tagger/x64/libTimeTaggerd.a'
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/'libs/Swabian_Instruments/Time_Tagger/x64/TimeTagger.lib'
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/'libs/Swabian_Instruments/Time_Tagger/x64/TimeTaggerd.lib'


################### DIGITAL I/O CARD M2i-7005 #############################

win32: LIBS += -L$$PWD/libs/Spectrum_Instr/c_header/ -lspcm_win64_msvcpp

INCLUDEPATH += $$PWD/libs/Spectrum_Instr/c_header
DEPENDPATH += $$PWD/libs/Spectrum_Instr/c_header

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/libs/Spectrum_Instr/c_header/spcm_win64_msvcpp.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/libs/Spectrum_Instr/c_header/libspcm_win64_msvcpp.a


################### ARBITRARY WAVEFORM GEN M8195A ##########################



win32: LIBS += -L$$PWD/libs/IVI_Foundation/IVI/Lib_x64/msc/ -lKtM8195

INCLUDEPATH += $$PWD/libs/IVI_Foundation/IVI/Include
DEPENDPATH += $$PWD/libs/IVI_Foundation/IVI/Include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/libs/IVI_Foundation/IVI/Lib_x64/msc/KtM8195.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/libs/IVI_Foundation/IVI/Lib_x64/msc/libKtM8195.a



################### LABJACK T7 MODULE ##########################



win32: LIBS += -L$$PWD/libs/LabJack/ -lLabJackM_x64

INCLUDEPATH += $$PWD/libs/LabJack
DEPENDPATH += $$PWD/libs/LabJack

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/libs/LabJack/LabJackM_x64.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/libs/LabJack/libLabJackM_x64.a
