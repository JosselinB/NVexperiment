# NVexperiment

Qt project used in lab to manage a confocal microscopy and quantum control experiment


### Instruments

The project is made to communicate with the following instruments:
(Manufacturer in bold)
- **PI** E-725 digital piezo stage controller
- **Keysight** M8195A 65GS/s arbitrary waveform generator
- **LabJack** T7 I/O DAQ module
- **Spectrum Instrumentation** M2i-7005 16channel digital I/O PCIe card
- **Swabian Instruments** TimeTagger20 pulse timing module
The lab project also contains
- **Standa** 8SMC5-USB motor controllers; not added yet, in part because code needs to be rewritten


### Graphical user interface

This project was developed using the Qt Creator IDE to be able to use the GUI designer that comes with it.

### Actual use

Using the code in real life to control instruments demands to link against the libraries implementing the instrument's API and that implies to have previously installed the corresponding software, which can be downloaded free of charge (not 100% sure for PI) from the manufacturers' websites or from me/our lab. 

This project is set up exclusively for Windows 10 (would probably work on Windows 11 though).

It compiles on MSVC2017 for x86_64bit architecture. Not really tested on any other machine as it is only actually run with the instrument on one workstation.


