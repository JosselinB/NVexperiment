#ifndef QCUSTOMPLOTWITHROI_H
#define QCUSTOMPLOTWITHROI_H
#include <QWidget>
#include "qcustomplot.h"
#include "qcprectroi.h"
#include "qcpcrosshair.h"
#include "iostream"

class QCPRectROI;

inline int roundToInt(double d) {return (int)(d+.5);}


typedef struct XYstagePos{
    int X;
    int Y;
} stagePos;

class QCustomPlotWithROI : public QCustomPlot
{
    Q_OBJECT
public:
    QCustomPlotWithROI(QWidget* parentWidget);
    virtual ~QCustomPlotWithROI();

    inline int ROInumber(){return ROIStore.size();}

    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);
    void wheelEvent(QWheelEvent* event);
    void mouseMoveEvent(QMouseEvent* event);

    double singleCellStepX();
    double singleCellStepY();

    QPointF getNextPixelCoords(QPointF point);
    QPointF getROIposition(int ROIid);
    QPointF getROIsize(int ROIid);

    //
    void enableROILegend(bool state);
    //

    QCPItemTracer* stagePosition;
    QCPItemText* YouAreHere;
    QCPColorMap* colorMap;


    QCPMovableStraightLine* lineH;
    QCPMovableStraightLine* lineV;

    QVector<QCPRectROI*> ROIStore;
    //
    bool aPressed = false;
    //

private:
    bool flag_BigStep;
    bool flag_VertSizeChange;
    bool enableLegend = false;

    //
    double curMousePosX;
    double curMousePosY;
    //

signals:
    void posMovedToX(double Xpos);
    void posMovedToY(double Ypos);
    void posMovedTo(double XPos, double YPos);

public slots:
    //
    void contextMenuRequest(QPoint pos);
    void addROI(QPointF ROICenter, double sizeX, double sixeY);
    //

    void addROI(QPointF ROICenter, int sizeX, int sixeY);
    void removeROI();
    //
    void removeAllROIs(bool state);
    //
    void setYouAreHereLabel(int ROIid);


    void enableCrosshair();
    void disableCrosshair();

private slots:
    void removeSelectedROI();
    void goToSelectedROI();
    void addROIbyClick(bool state);

};

#endif // QCUSTOMPLOTWITHROI_H
