#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <filesystem>
#include "qcustomplot.h"
#include "trigsequencegen.h"



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class ScanAndCount;
class LoadFileCreatMosaic;
class QSeqEditor;
class M2iCard;
class M8195Adev;

struct edgeProp;
//class fittingroutines;
class LabJackT7;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    std::vector<QSeqEditor*> seqEditors_in_GUI;
    std::vector<QCheckBox*> constStates_in_GUI;

    ScanAndCount* ScanAndCount_var;
    M2iCard* M2i7005_var;
    M8195Adev* M8195A_var;
    LabJackT7* LJ_T7;
    trigSequenceGen triggerGen;

    //fittingroutines* fittingroutines_var;
    //magnetcontrolclass *magnet;

    //for error message handling from E-725 piezo controller
    char szErrorMessage[1024];
    int	iError;
    //char bufferDataRec[600];
    QCPColorScale* colorScale3Dplot;
    QCPColorScale* colorScale2Dplot;
    LoadFileCreatMosaic* LoadFileCreatMosaic_var;

    bool GeneratorOn;
    std::filesystem::path globalPath;

    //maybe we don't need these 2 QVectors because this data is already contained in the colorMap one way or another
    //but it is simpler to just put it back there because access to data in colorMap is more complicated than it should be
    QVector<double> keyVec;
    QVector<double> valueVec;


    //For functionalities of the LabJack T7 acquisition module
    QTimer* acquisitionTimerFast;
    QTimer* acquisitionTimerSlow;
    int time_step_acq_fast; //update rate of the acquisition
    int time_step_acq_slow; //update rate of the acquisition
    //Note: the ADC of the LabJack T7 can stream at 100kSa/s whereas reading a single
    //value and sending it over Ethernet takes a few ms

    size_t data_length_LJ; // max number of displayed values

    //########################################################
    //the vectors below obviously cause redundantly stored data as the points are also stored in the QCustomPlot object
    //but since it is such a pain to work with the interface to the data of the QCustomPlot object
    //we just store a copy of it and use the setData method
    //########################################################
    QVector<double> time_vals_fast; // std::vector containing the x axis i.e. time values
    QVector<double> data_to_plot_fast; // container used to store the acquired analog data read out
    QVector<double> time_vals_slow;
    QVector<double> data_to_plot_slow;


    //Functions related to visualization of scan and crosshair use and ROI use
    void plotAfterDataAcq();
    void enableCrosshair();
    void disableCrosshair();
    void fillKeyVec();
    void fillValueVec();

    QVector<double> getHData();
    QVector<double> getVData();
    void replotAlongHLine();
    void replotAlongVLine();

    void wavGenTableLineFill(int lineNbInTable, int pxNumber, int stepSize, QString st);
    bool closedLoopCheck();
    void moveToPosition(double xPos, double yPos);
    void doesPathExist_ifNotCreate();

    bool writeSeqToFile(int nbActChan, int length, double tStep_in_ns, const std::vector< std::vector<edgeProp> >& risingE_Vec, const std::vector< std::vector<edgeProp> >& fallingE_Vec, const std::string& path, const std::string& name);
    bool loadSeqFromFile(std::filesystem::path filePath, std::string filename, std::vector< std::vector<edgeProp> >& risingE_outVec, std::vector< std::vector<edgeProp> >& fallingE_outVec);

    void pulseOutput();


    //Running repeated experiments
    void prepareRabiExp(std::vector<double> excTs, double readOutTime, bool ref, double piTime);

private slots:
    //function working without a ScanAndCount instance
    void on_crossHairBox_stateChanged(int newState);
    void on_setROILegend_clicked(bool checked);
    void on_addROI_clicked();
    void on_removeROI_clicked();
    void on_resetAxes_clicked();

    void on_LoadDATFile_clicked();
    void on_setMaxPlot_clicked();
    void on_setMaxPlotAuto_clicked();
    void on_PDFsave_clicked();
    void on_PNGsave_clicked();


    //functions that need the ScanAndCount instance
    void on_getCountsPerSec_clicked();
    void on_readValues_clicked();
    void on_STOPButton_clicked();
    void on_loopX_stateChanged(int st);
    void on_loopY_stateChanged(int st);
    void on_loopZ_stateChanged(int st);
    void on_setValX_editingFinished();
    void on_stepX_valueChanged(int step);
    void on_setValY_editingFinished();
    void on_stepY_valueChanged(int step);
    void on_setValZ_editingFinished();
    void on_stepZ_valueChanged(int step);
    void on_TwoDScanSetup_clicked();
    void on_TwoDScanStart_clicked();
    void on_ThreeDScanStart_clicked();
    void on_WaveTableRateButton_clicked();
    //void on_MosaicScanStart_clicked();

    void on_LoadDatFile3DScan_clicked();
    void on_setMaxPlot_3D_clicked();
    void on_setMaxPlot_3D_auto_clicked();
    void on_PNGsave3D_clicked();
    void on_PDFsave3D_clicked();

    void on_STOPbutton_clicked();
    void on_RUNbutton_clicked();
    void on_loadWaveformAndSeqTable_clicked();
    void on_triggerMode_currentIndexChanged(const QString &arg1);


    // Digital I/O stuff
    void on_initM2i_clicked();
    void on_confM2i_clicked();
    void on_loadData_clicked();
    void on_outputM2i_clicked();
    void on_closeM2i_clicked();
    void on_setPermWord_clicked();

    //Sequence editor stuff
    void on_writeFileTestButton_clicked();
    void on_loadFileTestButton_clicked();
    void on_parseButton0_clicked();
    void on_parseButton1_clicked();
    void on_parseButton2_clicked();
    void on_parseButton3_clicked();
    void on_parseButton4_clicked();
    void on_parseButton5_clicked();
    void on_parseButton6_clicked();
    void on_parseButton7_clicked();
    void on_changeSeqParams_clicked();


    //LabJack T7 stuff
    void acquireDataAndPlotFast();
    void acquireDataAndPlotSlow();

    void on_connectLabjack_clicked();
    void on_disconnectLabjack_clicked();
    void on_startMeasFast_clicked();
    void on_stopMeasFast_clicked();
    void on_startMeasSlow_clicked();
    void on_stopMeasSlow_clicked();
    void on_extDACvalue0_valueChanged(double arg1);
    void on_extDACvalue1_valueChanged(double arg1);
    void on_stepSizeExtDAC0_valueChanged(double arg1);
    void on_stepSizeExtDAC1_valueChanged(double arg1);
    void on_pulseOut_clicked();


    /*
    void on_SingleLorentzianFit_clicked();
    void on_DoubleLorentzianFit_clicked();
    void on_DampedSinusFit_clicked();
    */
};
#endif // MAINWINDOW_H
