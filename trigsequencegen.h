#ifndef EXPTRIGSEQUENCE_H
#define EXPTRIGSEQUENCE_H
#include <vector>

//the mapping from name of this enum to actual channel physically
//on the M2i-7005 card has to correspond
enum DIOchannel
{
    laserEnable,    //0
    laserDigitalMod,//1
    TimeTagger20,     //2
    AWGTriggerIn,   //3
    AWGEventIn,     //4
    sequStart       //5
};

class trigSequenceGen
{
public:
    trigSequenceGen();

    int channelNb;
    int timeStep_DIO_in_ns;

    std::vector< std::vector<int> > risingEdges;
    std::vector< std::vector<int> > fallingEdges;

    void adjust_first_rising(int offset);
    int ns_to_steps(double t);
    void eraseAllEdges();
    void appendLaserPulse(int start, int length);
    void CWODMR(double integTime);
    void pulsedODMR(double readOutTime, double piPulseTime);
    void RabiExp(double T, double readOutTime, bool ref, double piTime);
    void RamseyExp_waitInAWG(double T, double piHalfTime, double readOutTime, bool ref, double piTime);
    void RamseyExp_waitInDIOcard(double T, double piHalfTime, double readOutTime, bool ref, double piTime);
};

#endif // EXPTRIGSEQUENCE_H
