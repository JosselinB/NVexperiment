#include "m8195adev.h"
#include <stdio.h>




M8195Adev::M8195Adev() : status(0), session(0), ErrorCode(0)
{
    char devAddr[] = "PXI0::12-0.0::INSTR";
    ViBoolean idQuery = VI_TRUE;
    ViBoolean reset   = VI_TRUE;

    status = KtM8195_InitWithOptions(devAddr, idQuery, reset, "QueryInstrStatus=true, Simulate=false, DriverSetup= Model=M8195A", &session);
    if(status)
    {
        // Initialization failed
        KtM8195_GetError(session, &ErrorCode, 255, ErrorMessage);
        printf("** InitWithOptions() Error: %d, %s\n", ErrorCode, ErrorMessage);
    }

    assert(session != VI_NULL);
    printf("M8195A Driver Initialized \n");

    // Read and output a few attributes
    // Return status checking omitted for example clarity
    // assert statements copy_pasted from the Keysight example code
    status = KtM8195_GetAttributeViString(session, "", KTM8195_ATTR_SPECIFIC_DRIVER_PREFIX, 127, str);
    assert(status == VI_SUCCESS);
    printf("DRIVER_PREFIX:      %s\n", str);
    status = KtM8195_GetAttributeViString(session, "", KTM8195_ATTR_SPECIFIC_DRIVER_REVISION, 127, str);
    assert(status == VI_SUCCESS);
    printf("DRIVER_REVISION:    %s\n", str);
    status = KtM8195_GetAttributeViString(session, "", KTM8195_ATTR_SPECIFIC_DRIVER_VENDOR, 127, str);
    assert(status == VI_SUCCESS);
    printf("DRIVER_VENDOR:      %s\n", str);
    status = KtM8195_GetAttributeViString(session, "", KTM8195_ATTR_SPECIFIC_DRIVER_DESCRIPTION, 127, str);
    assert(status == VI_SUCCESS);
    printf("DRIVER_DESCRIPTION: %s\n", str);
    status = KtM8195_GetAttributeViString(session, "", KTM8195_ATTR_INSTRUMENT_MODEL, 127, str);
    assert(status == VI_SUCCESS);
    printf("INSTRUMENT_MODEL:   %s\n", str);
    status = KtM8195_GetAttributeViString(session, "", KTM8195_ATTR_INSTRUMENT_FIRMWARE_REVISION, 127, str);
    assert(status == VI_SUCCESS);
    printf("FIRMWARE_REVISION:  %s\n", str);
    status = KtM8195_GetAttributeViString(session, "", KTM8195_ATTR_SYSTEM_SERIAL_NUMBER, 127, str);
    assert(status == VI_SUCCESS);
    printf("SERIAL_NUMBER:  %s\n", str);

    // Reset Instrument
    //status = KtM8195_reset(session); //seems to be done at the initialisation of the instrument

    ViConstString Channel = "1";
    //hardcoded setting up the instrument
    ViInt32 ExtendedChCount = 1;  // 0: Internal, 1: Extended
    status = KtM8195_WaveformConfigureSingleChannel(session, ExtendedChCount, KTM8195_VAL_SAMPLE_RATE_DIVIDERDIV1);
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
    }

    // Valid range for sample rate is about 56GSa/s..64GSa/s.
    //status = KtM8195_SetAttributeViReal64(session, ChannelName, KTM8195_ATTR_ARB_SAMPLE_RATE, sRate);
    status = KtM8195_ConfigureSampleRate(session, maxSampleRate);
    printf("Sample Rate (GS/s):   %.2e\n", maxSampleRate);

    // Set Amplitude
    status = KtM8195_ArbitrarySetAmplitude(session, Channel, 0.8);
    printf("Set Amplitude (Volts):    %f\n", 0.8);

    // Set Offset to 0.0 Volts
    status = KtM8195_ArbitrarySetOffset(session, Channel, 0.);
    printf("Set Offset (Volts):        %f\n", 0.);

     //Set Termination Voltage to 0
     status = KtM8195_ArbitrarySetTerminationVoltage(session, Channel, 0.);
     printf("Set Termination Voltage (Volts):        %f\n", 0.);
     std::cout << std::endl;

     std::cout.precision(12);

}

M8195Adev::~M8195Adev()
{
    // Close the driver
    status = KtM8195_close(session);
    assert(status == VI_SUCCESS);

    printf("M8195A Driver closed \n");
}

//this will be good for a Rabi experiment or pulsed ODMR
ViStatus M8195Adev::loadSineArrayWaveformAndSeqTable(ViConstString channel, const std::vector<double>& freqs, double sample_freq, const std::vector<double>& excitation_times, ViInt64 scenLoopCount, const std::vector<double>& phases, const std::vector<double>& amplitudes)
{
    // Ensure instrument is stopped
    status = KtM8195_AbortGeneration(session);

    //the following scheme needs the generator to be in Scenario mode
    // the scenario is a sequence of sequences ; each sequence has 2 segments : the loopable and the rest segment
    status = KtM8195_ArbitrarySetSequencingMode(session, KTM8195_VAL_SEQUENCING_MODESTS_CENARIO);
    std::cout << "Generation mode set to SCENARIO mode" << std::endl;

    //CLEAR THE WAVEFORM MEMORY AND SEQUENCE TABLE FIRST
    status = KtM8195_ClearArbMemory(session);
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
        return status;
    }
    std::cout << "Cleared waveform memory" << std::endl;

    status = KtM8195_SequenceTableReset(session);
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
        return status;
    }
    std::cout << "Cleared sequence table" << std::endl;

    ViInt32 tableParam[6] = {0,0,0,0,0,0};

    //get granularity setting
    ViInt32 resSRD = 0;
    int SRD;
    status = KtM8195_InstrumentGetSampleRateDividerExtendedMemory(session, &resSRD);
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
        return status;
    }
    switch(resSRD)
    {
        case KTM8195_VAL_SAMPLE_RATE_DIVIDERDIV1: SRD = 1; break;
        case KTM8195_VAL_SAMPLE_RATE_DIVIDERDIV2: SRD = 2; break;
        case KTM8195_VAL_SAMPLE_RATE_DIVIDERDIV4: SRD = 4; break;
    }

    std::cout << "Sample Rate Divider: " << SRD << std::endl;

    int granularity = maxGranularity/SRD;
    int minSegmentSize = maxMinimumSegmentLength/SRD;

    int seg_length = 0;
    int seqIDCounter = 0;
    ViReal64 value = 0;
    ViInt32 segID = 0;
    std::vector<ViReal64> waveform;
    uint64_t totalSa = 0;
    int d;

    for (uint32_t k = 0; k < freqs.size(); ++k)
    {
        //std::cout << "Counter sequence ID: " << seqIDCounter << std::endl;
        //returns a triplet of {loop number, number of samples in loopable segment, sample number of rest segment} for a given signal_freq and excitation time
        int_triplet scanParamResult = sequenceParamsForSinePulseWGran(granularity, freqs[k], sample_freq, excitation_times[k], 1./std::pow(2.,9.));

        std::cout << "k=" << k << ", frequency: " << freqs[k]/1.e6 << " MHz" << ", loops-> " << scanParamResult.first << '\n' << "main seg: " << (double)scanParamResult.second/(1024*1024) << "MiSa; rest seg: " << (double)scanParamResult.third/(1024*1024)  << " MiSa" << std::endl;

        double freq_factor = 2. * dPI * freqs[k] / sample_freq;

        //(re)set the waveform vector to size 0
        waveform.resize(0);
        //add a loopable segment if one is needed i.e the excitation time is longer than the length of one minimum loopable segment

        if (scanParamResult.first != 0) // CASE WHERE THERE IS A LOOPABLE/MAIN SEGMENT + A REST SEGMENT
        {
            //-------------------------
            //for the loopable segment
            //-------------------------
            // calculate the values of the samples and put them into a vector
            // granularity criterion should be satisfied from the method of calculating the minimum length

            int min_seg_length_with_granularity = scanParamResult.second;

            if (min_seg_length_with_granularity < minSegmentSize)//satisfy min segment size of 1280Samples
            {
                min_seg_length_with_granularity = ((minSegmentSize/min_seg_length_with_granularity)+1)*min_seg_length_with_granularity;
            }

            //fill the vector containing the sample data i.e. the waveform itself (floating point value between between 1. and -1.)
            for (int i =0; i<min_seg_length_with_granularity; ++i)
            {
                //value = (ViReal64)(std::sin(freq_factor * ((double)i + startTime_factor) + phase_factor));
                value = amplitudes[k]*(ViReal64)(std::sin(freq_factor * (double)i + phases[k]));
                waveform.push_back(value);
            }

            // Final granularity check !
            d = waveform.size()/granularity;
            assert(("Waveform length segment is not a multiple of granularity !", d*granularity==waveform.size()));

            // Define and download the loopable segment
            status = KtM8195_WaveformCreateChannelWaveform(session, channel, waveform.size(), waveform.data(), &segID);
            if(status != KTM8195_SUCCESS)
            {
                status = KtM8195_error_message(session, status, ErrorMessage);
                std::cout << "### ERROR ### " << ErrorMessage << std::endl;
                status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
                assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
                std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
                return status;
            }

            totalSa += waveform.size();

            std::cout << "segment ID " << segID << std::endl;
            assert(("Handle/segment ID return to equals 0, which is weird...", segID != 0));

            if (scanParamResult.third > 0) // if there is a rest segment
            {
                tableParam[0] = init_sequence + sequence_adv_repeat + segment_adv_auto;
            }
            else //there is NO rest segment i.e. only a loopable segment in that case
            {
                if(k==freqs.size()-1)//last frequency in the array so we have to add the end scenario marker
                {
                    tableParam[0] = init_sequence + end_sequence + sequence_adv_repeat + segment_adv_auto + end_scenario;  // Control word
                }
                else
                {
                    tableParam[0] = init_sequence + end_sequence + sequence_adv_repeat + segment_adv_auto; // Control word
                }
            }

            // set the parameters for the segment in the sequence table
            tableParam[1] = 1;                                          // Sequence Loop Count->1
            tableParam[2] = scanParamResult.first;                      // Segment Loop Count
            tableParam[3] = segID;                                      // Segment ID
            tableParam[4] = 0;                                          // Segment Start Offset
            tableParam[5] = waveform.size() - 1;                        // Segment End Offset
            // FOR THE SEGMENT END OFFSET, THE VALUE 0xFFFFFFFF IS NOT VALID HERE
            // AS OPPOSED TO THE CASE OF THE SCPI COMMAND (and as stated in the manual) !
            // the instrument throws an error when given 0xFFFFFFFF

            status = KtM8195_SequenceTableSetData(session, seqIDCounter, (ViInt32)6, tableParam);
            if(status != KTM8195_SUCCESS)
            {
                status = KtM8195_error_message(session, status, ErrorMessage);
                std::cout << "### ERROR other params ### " << ErrorMessage << std::endl;
                status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
                assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
                std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
                return status;
            }

            //std::cout << "Defined entry of size " << waveform.size() << " in seq table at SeqID " << seqIDCounter << std::endl;
            ++seqIDCounter;

            seg_length = scanParamResult.third;

            if(seg_length > 0)
            {
                //-------------------------
                //for the rest segment
                //-------------------------
                //assert(("Rest segment is larger than loopable segment!", scanParamResult.third <= scanParamResult.second));
                if (waveform.size() < scanParamResult.second)
                {
                    std::cout << "#### Rest segment is larger than loopable segment! A bit suprising... (because of rounding probably) ####\n";
                }

                // Define and download the rest segment
                // first cut down to the right size of samples
                waveform.resize(seg_length);
                //now adjust the number of sample in rest segment to granularity
                //which means pad with 0 to the next multiple of the granularity
                //and fufill the criterion of minimum segment size
                size_t adj_seg_length = std::max((seg_length/granularity + 1)*granularity, minSegmentSize);
                waveform.resize(adj_seg_length, 0);

                // Final granularity check !
                d = waveform.size()/granularity;
                assert(("Waveform length segment is not a multiple of granularity !", d*granularity==waveform.size()));

                status = KtM8195_WaveformCreateChannelWaveform(session, channel, waveform.size(), waveform.data(), &segID);

                if(status != KTM8195_SUCCESS)
                {
                    status = KtM8195_error_message(session, status, ErrorMessage);
                    std::cout << "### ERROR ### " << ErrorMessage << std::endl;
                    status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
                    assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
                    std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
                    return status;
                }
                totalSa += waveform.size();
                std::cout << "segment ID " << segID << std::endl;
                //std::cout << "Uploaded " << waveform.size() << " samples as rest segment with ID " << segID << std::endl;
                assert(("Handle/segment ID return to equals 0, which is weird...", segID != 0));

                if(k==freqs.size()-1)//last frequency in the array so we have to add the end scenario marker
                {
                    tableParam[0] = end_sequence + segment_adv_auto + end_scenario; // Control word
                }
                else
                {
                    tableParam[0] = end_sequence + segment_adv_auto; // Control word
                }

                tableParam[1] = 1;              // Sequence Loop Count->1
                tableParam[2] = 1;              // Segment Loop Count
                tableParam[3] = segID;          // Segment ID
                tableParam[4] = 0;              // Segment Start Offset
                tableParam[5] = waveform.size() - 1;     // Segment End Offset

                status = KtM8195_SequenceTableSetData(session, seqIDCounter, (ViInt32)6, tableParam);
                if(status != KTM8195_SUCCESS)
                {
                    status = KtM8195_error_message(session, status, ErrorMessage);
                    std::cout << "### ERROR ### " << ErrorMessage << std::endl;
                    status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
                    assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
                    std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
                    return status;
                }
                //std::cout << "Defined " << waveform.size() << " entry in seq table with SeqID " << seqIDCounter << std::endl;
                ++seqIDCounter;
            }

        }
        else // THERE IS ONLY ONE (REST) SEGMENT FOR THIS FREQUENCY AND EXCITATION TIME
        {
            seg_length = scanParamResult.third;
            for (int i =0; i<seg_length; ++i)
            {
                value = amplitudes[k]*(ViReal64)(std::sin(freq_factor * (double)i + phases[k]));
                waveform.push_back(value);

            }

            //now adjust the number of sample in rest segment to granularity
            //which means pad with 0 to the next multiple of the granularity
            //and fulfill the criterion of minimum segment size
            size_t adj_seg_length = std::max((seg_length/granularity + 1)*granularity, minSegmentSize);
            waveform.resize(adj_seg_length, 0);

            // Final granularity check !
            d = waveform.size()/granularity;
            assert(("Waveform length segment is not a multiple of granularity !", d*granularity==waveform.size()));

            // Define and download the loopable segment
            status = KtM8195_WaveformCreateChannelWaveform(session, channel, waveform.size(), waveform.data(), &segID);
            if(status != KTM8195_SUCCESS)
            {
                status = KtM8195_error_message(session, status, ErrorMessage);
                std::cout << "### ERROR ### " << ErrorMessage << std::endl;
                status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
                assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
                std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
                return status;
            }
            totalSa += waveform.size();
            std::cout << "segment ID " << segID << std::endl;
            //std::cout << "Uploaded " << waveform.size() << " samples as single segment with ID " << segID << std::endl;
            //assert(("Handle/segment ID return to equals 0, which is weird...", segID != 0));

            if(k==freqs.size()-1)//last frequency so we have to add the end scenario marker
            {
                tableParam[0] = init_sequence + end_sequence + segment_adv_auto + sequence_adv_repeat +  end_scenario; // Control word
            }
            else
            {
                tableParam[0] = init_sequence + end_sequence + segment_adv_auto + sequence_adv_repeat; // Control word
            }
            tableParam[1] = 1;              // Sequence Loop Count->1
            tableParam[2] = 1;              // Segment Loop Count
            tableParam[3] = segID;          // Segment ID
            tableParam[4] = 0;              // Segment Start Offset
            tableParam[5] = waveform.size() - 1;     // Segment End Offset
            status = KtM8195_SequenceTableSetData(session, seqIDCounter, (ViInt32)6, tableParam);
            if(status != KTM8195_SUCCESS)
            {
                status = KtM8195_error_message(session, status, ErrorMessage);
                std::cout << "### ERROR ### " << ErrorMessage << std::endl;
                status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
                assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
                std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
                return status;
            }
            //std::cout << "Defined " << waveform.size() << " entry in seq table with SeqID " << seqIDCounter << std::endl;

            ++seqIDCounter;
        }


        std::cout << "Total number of samples uploaded: " << (double)totalSa/(1024*1024) << " MiSa." << std::endl;
        if(totalSa>(1ULL<<34)) //being cautious with implicit casting between 32 and 64 bit;
                                                //1ULL<<34 <=> 2^34=16*1024*1024*1024 = 16Gi
        {
            std::cout << "ERROR: Memory limit of 16 GiSa has been exceeded! The sequence data is now INVALID." << std::endl;
            std::cout << "Please reupload a smaller sequence." << std::endl;
            return status;
        }
    }

    //now set the right parameters for the scenario
    status = KtM8195_ScenarioSetAdvancementMode(session, KTM8195_VAL_ADVANCEMENT_MODE_SINGLE);
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
        return status;
    }

    status = KtM8195_ScenarioSetLoopCount(session, scenLoopCount);
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
        return status;
    }

    status = KtM8195_ScenarioSetStartIndex(session, 0); //in this case we make the scenario always start at sequenceID = 0
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
        return status;
    }

    //enable output on channel 1
    ViConstString Channel = "1";
    status = KtM8195_ConfigureOutputEnabled(session, Channel, VI_TRUE);
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
        return status;
    }

    return status;
}


ViStatus M8195Adev::stopGen()
{
    status = KtM8195_AbortGeneration(session);
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
        return status;
    }
    std::cout << "Generator should be stopped" << std::endl;
}

ViStatus M8195Adev::run()
{
    status = KtM8195_InitiateGeneration(session);
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
        return status;
    }
    std::cout << "Generator should be running" << std::endl;
}

ViStatus M8195Adev::setTriggermode(const QString &mode)
{
    if(mode == "Triggered")
    {
        std::cout << "Trigger mode selected" << std::endl;
        status = KtM8195_TriggerConfigureMode(session, KTM8195_VAL_ARM_MODE_SELF, VI_FALSE, VI_FALSE);
    }
    else if (mode == "Gated")
    {
        std::cout << "Gated mode selected" << std::endl;
        status = KtM8195_TriggerConfigureMode(session, KTM8195_VAL_ARM_MODE_SELF, VI_TRUE, VI_FALSE);
    }
    else
    {
        std::cout << "Continuous mode selected" << std::endl;
        status = KtM8195_TriggerConfigureMode(session, KTM8195_VAL_ARM_MODE_SELF, VI_FALSE, VI_TRUE);
    }
    if(status != KTM8195_SUCCESS)
    {
        status = KtM8195_error_message(session, status, ErrorMessage);
        std::cout << "### ERROR ### " << ErrorMessage << std::endl;
        status = KtM8195_error_query(session, &ErrorCode, ErrorMessage);
        assert(("Error querying the error: looks like you're f***ed.\n", status == KTM8195_SUCCESS));
        std::cout << "ERROR " << ErrorCode << " --> " <<  ErrorMessage << std::endl;
        return status;
    }

    return status;
}
